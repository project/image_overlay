<?php

define("IMAGE_OVERLAY_FIXED_ALL", 0); 
define("IMAGE_OVERLAY_FIXED_RATIO", 1);
define("IMAGE_OVERLAY_FIXED_NONE", 2);


//include(drupal_get_path('module', 'image_overlay') .'/drawing_shapes.inc');

/**
 * image_overlay_drawing_split_points
 * Takes an array of point pairs and returns an array of points. ints & floats ok.
 * { {x1, y1}, {x2, y2}, {x3, y3}, {x4, y4} } to 
 * { x1, y1, x2, y2, x3, y3, x4,y4 }
 * 
 * Converts any invalid number into a zero
 */
function image_overlay_drawing_split_points( $arr, $num ) {
  $ans = array();
  $string = '';
  for ($i = 0, $j = 0; $i < count($arr); $i++ ) {
    $t = $arr[$i];
    $ans[$j++] = $t[0];
    $ans[$j++] = $t[1];
  }
  return $ans;
}


/**
 * image_overlay_drawing_parse_color
 *
 * @param $im  the image to allocate the color to
 * @param $color a string in the form "rgb(255, 255, 255)" or "#FFFFFF" or "FFFFFF"
 * @return the color allocated using imagecolorallocate. If color cannot be read, allocates black
 */
function image_overlay_drawing_parse_color(&$im, $color) {
  $ans = NULL;
  $numbers = array();

  if (substr_count($color, "rgb")) {
    $numbers = preg_split("/[rgb,[)(\]]+/", $color, -1, PREG_SPLIT_NO_EMPTY);
  }
  else {
    $arr = array();
    preg_match_all("/([0-9a-fA-F][0-9a-fA-F])([0-9a-fA-F][0-9a-fA-F])([0-9a-fA-F][0-9a-fA-F])/", $color, $arr);
    $count = count($arr);
    for ($i=1; $i<$count;$i++) {
      $numbers[$i-1] = hexdec($arr[$i][0]);
    }
  }
  
  if (count($numbers) < 1) {
    $numbers[0] = 0;
    $numbers[1] = 0;
    $numbers[2] = 0;
  }
  return imagecolorallocate($im, $numbers[0], $numbers[1], $numbers[2]);
}


/**
 * image_overlay_drawing_create_image
 * Actually makes the image
 */
function image_overlay_drawing_create_image($extension, $image_base) {
	if ( ! is_file($image_base) ) {
		watchdog("noimage", "$image_base  is not a file");
	}

  if ( (! strcmp($extension, "jpg")) || (!strcmp($extension, "jpeg"))) {
    return imagecreatefromjpeg($image_base);
  }
  if (! strcmp($extension, "png")) {
    return imagecreatefrompng($image_base);
    }
    if (! strcmp($extension, "gif")) {
    return imagecreatefromgif($image_base);
  }
  
  /*      $imagefunction = 'imagecreatefrom' . $bgimagetype;
      $im = $imagefunction($backgroundimage);
*/
}

function image_overlay_drawing_save_image(&$im, $new_name, $extension) {
  watchdog('image', "new_name in image_overlay_drawing_save_image '$new_name'");
    $imagefunction = "image$extension";
    $imagefunction($im, $new_name);
}

function image_overlay_drawing_cleanup_image(&$im) {
  imagedestroy($im);
}

function _image_overlay_shape_string($obj) {
}

function _image_overlay_revise_image($coords, $blank_image_file, $file, $extension, $original_nid, $info){
  // blank_image_file is something like : "sites/default/files/images/test.png" 
  // file is something like : "sites/default/files/images/test_overlay.png" 

  $slash = strrpos($file, '/');
  $last_dot = strrpos($file, '.');
  $length = $last_dot - $slash - 1;

  $new_file['name'] = substr($file, $slash+1, $length);
  $new_file['dir'] = substr($file, 0,  $slash+1);
    watchdog("image", "In _image_overlay_revise_image : dir is '" . $new_file['dir'] . "', and name is '" . $new_file['name'] . "' Also file is '$file' and oldfile is '$blank_image_file' and length is $length. slash is at $slash and last_dot is at $last_dot");

  return _image_overlay_create_image($coords, $blank_image_file, $new_file, $extension, $original_nid, $info);
}


function _image_overlay_make_image($coords, $file, $extension, $original_nid, $info){

  $slash = strrpos($file, '/');
  $last_dot = strrpos($file, '.');
  $length = $last_dot - $slash - 1;
  
  $new_file['name'] = substr($file, $slash+1, $length) . '_overlay';
  $new_file['dir'] = file_create_path(file_directory_path() .'/'. variable_get('image_default_path', 'images'));

  return _image_overlay_create_image($coords, $file, $new_file, $extension, $original_nid, $info);
}

function _image_overlay_create_image($coords, $base_file, $new_file, $extension, $original_nid, $info) {

  $obj = json_decode($coords);
  $canvases =  $obj->BGObject->Canvases;

  $new_name =  '';
  $new_file['type'] = strtolower($extension);
  
  if (!strcmp($new_file['type'], "jpg")) {
    $new_file['type'] = 'jpeg';
    $extension = 'jpeg';
  }
  if ( !strcmp($new_file['type'], "gif")){
    $new_file['type'] = 'png';
  }

  $new_name =  $new_file['dir'] . $new_file['name'] .'.'. $new_file['type'];
watchdog("image", "$new_name  ithe new name in _image_overlay_create_image");

  $im = image_overlay_drawing_create_image($extension, $base_file);
  
  for ($i = 1; $i < count($canvases); $i++) {
    image_overlay_drawing_shape($im, $canvases[$i-1]);
  }

  image_overlay_drawing_save_image($im, $new_name, $new_file['type']);
  image_overlay_drawing_cleanup_image($im);
  return $new_file;
}

//_image_overlay_read_shape( )

/*
function _image_overlay_get_num( ) {
  $ans = variable_get('image_overlay_count', 0);
  if ( $ans > 100 ) {
    variable_set('image_overlay_count', 1);
  }
  return $ans;
}

function _image_overlay_find_int( $a1 ) {
  static $pattern = "/[\-]?[0-9]*{\.}?[0-9]/";
  $result = preg_match_all($pattern, $a1);
  if ( !$result ) {
    return 0;
  }
  return to_float($a1);
}*/


function _image_overlay_load_fonts ( ) {
  $modulepath = drupal_get_path('module', 'image_overlay');

  // search drupal's base dir, files dir, and image_overlay/fonts
  $fontpath = array(getcwd() . '/' . $modulepath .  '/fonts/','.', realpath('./fonts/'), file_directory_path(), $modulepath . '/fonts/' );
  $path_delimiter = (substr(PHP_OS, 0, 3) == 'WIN') ? ';' : ':';
  putenv('GDFONTPATH=' . implode($path_delimiter, $fontpath));
  return getcwd() . '/' . $modulepath .  '/fonts';
}

function _image_overlay_get_data( $shape_id ) {
  $shape_files = array();
  if ( !isset($shape_files[$shape_id]) ) {
    $shape_files[$shape_id] = db_result(db_query(db_rewrite_sql("SELECT data FROM {overlay_shape_types} WHERE shape_id = %d"),$shape_id));
  }
  return $shape_files[$shape_id];
}
  
function image_overlay_get_shape( $shape_id) {
  $result = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {overlay_shape_types} WHERE shape_id = %s"), $shape_id));
  
  return array(
    'id' => $result->shape_id,
    'type' => $result->shape_type,
    'name' => $result->shape_name,
    'height' => $result->shape_height,
    'width' => $result->shape_width,
    'fixed' => $result->fixed_size,
    'data' => $result->data
  );
}
  
function _image_overlay_get_custom_shapes(  ) {

  $shape_types = array();
  $shape_types['polygon'] = array();
  $shape_types['font'] = array();
  $shape_types['image'] = array();
  $sql = db_rewrite_sql("SELECT * FROM {overlay_shape_types} WHERE shape_type = '%s'");


  $polygon_query = db_query($sql, "polygon");
  while($polygon = db_fetch_object($polygon_query)) {
    $shape_types['polygon'][$polygon->shape_id] = array(
      'id' => $polygon->shape_id,
      'name' => $polygon->shape_name,
      'height' => $polygon->shape_height,
      'width' => $polygon->shape_width,
      'fixed' => $polygon->fixed_size,
      'data' => $polygon->data
    );
  }

  $font_query = db_query($sql, "font");
  while($font = db_fetch_object($font_query)) {
    $shape_types['font'][$font->shape_id] = array(
      'id' => $font->shape_id,
      'name' => $font->shape_name,
      'size' => $font->shape_height,
      'family' => $font->shape_width,
      'file' => $font->data
    );
}
 
 $image_query = db_query($sql, "image");
  while($image = db_fetch_object($image_query)) {
    $shape_types['image'][$image->shape_id] = array(
      'id' => $image->shape_id,
      'name' => $image->shape_name,
      'height' => $image->shape_height,
      'width' => $image->shape_width,
      'fixed' => $image->fixed_size,
      'file' => $image->data
    );
}
  
  return $shape_types;
}  