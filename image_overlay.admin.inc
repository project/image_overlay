<?php

 function _image_overlay_ratios() {
   //        Can the aspect ratio or the size be changed? If no to both, 0. If yes to size, 1. If yes to both, 2. 

  $fixed_options = array(
    '0' => 'Both',
    '1' => 'Ratio',
    '2' => 'None',
  );
 return $fixed_options;
} 
 
 function _image_overlay_families() {
   $font_families = array(
    '3' => 'Cursive',
    '4' => 'Fantasy',
    '5' => 'Monospace',
    '2' => 'Sans-Serif',
    '1' => 'Serif',
  );
  return $font_families;
}
 
 
function image_overlay_admin_settings( ) {
  
  $form['#id'] = 'image_overlay_admin';
  $form['#submit'][] = 'image_overlay_admin_settings_submit';

  $form['main'] = array(
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
    '#collapsible' => TRUE,
    '#weight' => -7,
    '#title' => t("Basic Configuration"),
    '#tree' => TRUE,
  );
  $form['main']['enable-ie'] = array( 
    '#type' => 'checkbox',
    '#title' => t('Enable Internet Explorer Script'),
    '#description' => t("Allows Internet Explorer users to add overlays to images."),
    '#return_value' => 1,
    '#default_value' => variable_get('image_overlay_ie_enable', 0),
  );
  $form['main']['temp-dir'] = array(
    '#type' => 'textfield',
    '#title' => 'Temporary Directory',
    '#description' => 'The directory to save temporary images into. Should already exist. End in"/". Example: "files/temp/".',
    '#value' => variable_get('image_overlay_temp_dir', 'files'),
    '#weight' => -8,
    '#id' => 'temp_dir',
  );
  
  /////////////////////
  
  
/**  $form['custom'] = array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#weight' => -4,
    '#title' => t("Other Options"),
    '#tree' => TRUE,
  );**/

  ////////////////////////////////////
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Save Settings',
    '#weight' => 9,
  );
  return $form;
}
  

function image_overlay_admin_settings_submit( $form, &$form_state ) {
  variable_set('image_overlay_temp_dir', $form_state['values']['main']['temp-dir']);
  variable_set('image_overlay_ie_enable', $form_state['values']['main']['enable-ie']);
 }


function image_overlay_admin_settings_fonts() {
  $shapes = _image_overlay_get_custom_shapes();
  $form['#id'] = 'image_overlay_admin_settings_fonts';
  $form['#submit'][] = 'image_overlay_admin_settings_fonts_submit';
  
  $font_families = _image_overlay_families();
  
  $header = array(t('Font Name'), t('File Name'), t('Default Size'), t('Family'), t('Edit'));
  $rows = array();

  //columns: 
  // shape_name, data, defualt size, family, edit
  foreach($shapes['font'] as $result) {
    $rows[] = array(
      $result['name'],
      $result['file'],
      $result['size'],
      $font_families[$result['family']], 
      l('configure', 'admin/settings/image_overlay/fonts/'.$result['id'])
    );
}

  $form['existing'] = array(
    '#value' =>  theme('table', $header, $rows),
    '#weight' => 1
  );

  $form['new'] = array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#title' => t('Add a new font'),
    '#tree' => TRUE,
  );

  _image_overlay_admin_font_form( $form, 'new', '', 12, 1, '');    

  $form['new']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Add font',
      '#weight' => 9,
    );
  return $form;
}

function image_overlay_admin_settings_fonts_submit($form, &$form_state) {
  $values = $form_state['values']['new'];
  $size = (int) $values['font_size'];
  if ( $size == 0 ) { $size = 10; }

  $out = print_r($values, TRUE);
  watchdog('imagedebug', $out);
  image_overlay_save_shape( $values['font_name'], 'font', $values['font_file'], $size, (int) $values['font_family']);

  $form_state['redirect'] = 'admin/settings/image_overlay/fonts';
  return $form_state['redirect'];
}


function image_overlay_admin_edit_font($form, $shape_id = NULL) {
  $form['#id'] = 'image_overlay_admin_edit_font';
  $form['#submit'][] = 'image_overlay_admin_edit_font_submit';

  if (is_numeric($shape_id)) {
    $font = image_overlay_get_shape($shape_id);

    $form['font'] = array(
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      '#weight' => -5,
      '#title' => t("Edit ") . $font['name'],
      '#tree' => TRUE,
    );  

    _image_overlay_admin_font_form($form, 'font', $font['name'], $font['height'], $font['width'], $font['data'], $font['id']);

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save Changes',
      '#weight' => 9,
    );
      return $form;
  }

  return $form;
}

function image_overlay_admin_edit_font_submit($form, &$form_state ) {
  $info = $form['values']['font'];
  //function image_overlay_update_shape( $shape_id, $name, $data, $height = 0, $width = 0, $fixed = 0) {
image_overlay_update_shape( $info['shape_id'], $info['font_name'], $info['font_file'], $info['font_size'], $info['font_family']);
    $form_state['redirect'] = 'admin/settings/image_overlay/fonts';
  return $form_state['redirect'];

//  db_query("UPDATE {overlay_shape_types} SET shape_name = '%s', data = '%s', shape_width = %d, shape_height = %d, WHERE shape_id = %d", $info['font_name'], $info['font_file'], $info['font_family'], $info['font_size'], $info['shape_id']);
}


/**
   * $size = INT,
   * $name = [EMPTY] STRING
   * $file = [EMPTY] STRING
   * $family = INT (see array) or NUMBER STRING
   *
   * $group is the fieldset or sub-array of the form where everything will be inserted.
   **/
function _image_overlay_admin_font_form( &$form, $group, $name, $size, $family, $file, $id = 0 ) {


  
  $form[$group]['font_name'] = array(
    '#type' => 'textfield',
    '#title' => "Font Name",
    '#description' => 'The font name should be the exact name of the font.',
    '#default_value' => $name,
    '#required' => TRUE,
    '#size' => 40,
    '#weight' => -10,
    '#id' => 'font_name',
  );
  $form[$group]['font_file'] = array(
    '#type' => 'textfield',
    '#title' => "Font File",
    '#description' => 'The font file must be the name of a ttf in the image_overlay/fonts directory, without the extension ttf ("FreeSerifA.ttf" becomes "FreeSerifA".)',    
    '#default_value' => $file,
    '#required' => TRUE,
    '#size' => 40,
    '#weight' => -8,
  );
    $form[$group]['font_size'] = array(
    '#type' => 'textfield',
    '#title' => "Default Size",
    '#description' => 'The default point size of the font.',    
    '#default_value' => $size,
    '#required' => TRUE,
    '#size' => 2,
    '#weight' => -7,
  );
  $form[$group]['font_family'] = array(
    '#type' => 'select',
    '#title' => "Font Type",
    '#default_value' => $family,
    '#required' => TRUE,
    '#options' => _image_overlay_families(),
    '#weight' => -4,
  );
  $form[$group]['font_id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
}


function image_overlay_admin_settings_symbols() {
  $shapes = _image_overlay_get_custom_shapes();
  $form['#id'] = 'image_overlay_admin_symbols';
  $form['#submit'][] = 'image_overlay_admin_settings_symbols_submit';
  $fixed_options = _image_overlay_ratios();
  
  $header = array(t('Name'), t('Fixed Ratio & Size'), t('Height'), t('Width'), t('File'), t('Edit'));
  $rows = array();

  //columns: 
  // shape_name, fixed, height, width, file, edit
  foreach($shapes['image'] as $result) {
    $rows[] = array(
      $result['name'],
      $fixed_options[$result['fixed']],
      $result['height'],
      $result['width'],
      $result['file'],
      l('configure', 'admin/settings/image_overlay/symbols/'.$result['id'])
    );
}

  $form['existing'] = array(
    '#value' =>  theme('table', $header, $rows),
    '#weight' => 1
  );

  $form['new'] = array(
    '#type' => 'fieldset',
    '#collapsed' => TRUE,
    '#collapsible' => TRUE,
    '#title' => t('Add a new symbol'),
    '#description' => t('A symbol that will be available as a shape when creating overlays.'),
    '#tree' => TRUE,
  );

  _image_overlay_admin_symbol_form( $form, 'new', '', 0, 0, 0, '');

  $form['new']['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Add symbol',
      '#weight' => 9,
    );
  return $form;
}

function image_overlay_admin_settings_symbols_submit($form, &$form_state) {
  $values = $form_state['values']['new'];
  watchdog("imagepics", "<PRE>" . print_r($values, TRUE) . "</pre>");
//function image_overlay_save_shape( $name, $type, $data, $height = 0, $width = 0, $fixed = 0) {
  image_overlay_save_shape( $values['symbol_name'], 'image', $values['symbol_file'], (int) $values['symbol_height'], (int) $values['symbol_width'], (int) $values['symbol_ratio']);

  $form_state['redirect'] = 'admin/settings/image_overlay/symbols';
  return $form_state['redirect'];
}


function image_overlay_admin_settings_edit_symbol($form, $shape_id = NULL) {
  $form['#id'] = 'image_overlay_admin_settings_edit_symbol';
  $form['#submit'][] = 'image_overlay_admin_settings_edit_symbol_submit';

  if (is_numeric($shape_id)) {
    $symbol = image_overlay_get_shape($shape_id);

    $form['symbol'] = array(
      '#type' => 'fieldset',
      '#collapsed' => FALSE,
      '#collapsible' => FALSE,
      '#weight' => -5,
      '#title' => t("Edit ") . $symbol['name'],
      '#tree' => TRUE,
    );  

    //function _image_overlay_admin_symbol_form( &$form, $group, $name, $height, $width, $int_ratio, $file, $id = 0 ) {
    _image_overlay_admin_symbol_form($form, 'symbol', $symbol['name'], $symbol['height'], $symbol['width'], $symbol['fixed'], $symbol['data'], $symbol['id']);


    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => 'Save Changes',
      '#weight' => 9,
    );
      return $form;
  }

}

function image_overlay_admin_settings_edit_symbol_submit($form, &$form_state ) {
  $info = $form['values']['symbol'];
  //function image_overlay_update_shape( $shape_id, $name, $data, $height = 0, $width = 0, $fixed = 0) {
  image_overlay_update_shape( $info['shape_id'], $info['symbol_name'], $info['symbol_file'], $info['symbol_height'], $info['symbol_width'], $info['symbol_ratio']);
  $form_state['redirect'] = 'admin/settings/image_overlay/symbols';
  return $form_state['redirect'];

//  db_query("UPDATE {overlay_shape_types} SET shape_name = '%s', data = '%s', shape_width = %d, shape_height = %d, WHERE shape_id = %d", $info['symbol_name'], $info['symbol_file'], $info['symbol_family'], $info['symbol_size'], $info['shape_id']);
}




function _image_overlay_admin_symbol_form( &$form, $group, $name, $height, $width, $int_ratio, $file, $id = 0 ) {
  // shape_name, fixed, height, width, file, edit
 
  $form[$group]['symbol_name'] = array(
    '#type' => 'textfield',
    '#title' => t("symbol Name"),
    '#description' => t('Something to identify this symbol'),
    '#default_value' => (string) $name,
    '#required' => TRUE,
    '#size' => 40,
    '#weight' => -10,
  );
  $form[$group]['symbol_file'] = array(
    '#type' => 'textfield',
    '#title' => t("symbol File"),
    '#description' => t('Should be in image_overlay/custom_images. Better interface in the works.'),    
    '#default_value' => $file,
    '#required' => TRUE,
    '#size' => 40,
    '#weight' => -8,
  );
    $form[$group]['symbol_height'] = array(
    '#type' => 'textfield',
    '#title' => t("Default Height"),
    '#description' => t('The default height of the symbol.'),    
    '#default_value' => $height,
    '#required' => TRUE,
    '#size' => 2,
    '#weight' => -7,
  );
  $form[$group]['symbol_width'] = array(
    '#type' => 'textfield',
    '#title' => t("Default width"),
    '#description' => t('The default width of the symbol.'),    
    '#default_value' => $width,
    '#required' => TRUE,
    '#size' => 2,
    '#weight' => -7,
  );
  $form[$group]['symbol_ratio'] = array(
    '#type' => 'select',
    '#title' => t("Fixed height/width or ratio?"),
    '#default_value' => $ratio,
    '#required' => TRUE,
    '#options' => _image_overlay_ratios(),
    '#weight' => -4,
  );
  $form[$group]['symbol_id'] = array(
    '#type' => 'hidden',
    '#value' => $id,
  );
}
