<?php

function image_overlay_save_overlay( $json, $overlay_id, $original_nid, &$new_node ) { 
  $coords_string = serialize($json);
  $time = $new_node->created;

  if ( $overlay_id == 0 ) {
    db_query("INSERT INTO {overlays} (nid, original_nid, data) VALUES (%d, %d, '%s')", $new_node->nid, $original_nid, $coords_string);
    $overlay_id = _image_overlay_overlay_id($new_node->nid);
  }
  else {
    db_query("UPDATE {overlays} SET data = '%s' WHERE overlay_id = %d", $coords_string, $overlay_id);
  }
  db_query("INSERT INTO {overlay_revisions} (nid, overlay_id, timestamp, data) VALUES (%d, %d, %d, '%s')", 
    $new_node->nid, 
    $overlay_id, 
    $new_node->created, 
    $coords_string
  );
}

/** 
 * image_overlay_load_overlay
 *
 * Returns an object $result:
 *    $result->nid               =    the nid the overlay is saved as 
 *    $result->original_nid       =    the id of the node the overlay is based on
 *    $result->data             =    the serialized overlay data
 *    $result->overlay_id       =    the id of the overlay
 *    $result->overlay_info     =    the object (unserialized data)
 **/
function image_overlay_load_overlay($current_nid) {
  /** DB Stuff **/
    $overlay = _image_overlay_overlay_id($current_nid);
    if ($overlay) {
      $result = db_fetch_object(db_query(db_rewrite_sql("SELECT * FROM {overlays} WHERE nid = %d"), $current_nid));
      $result->overlay_info = unserialize($result->data);
      return $result;
    }
    return false;
}

function image_overlay_save_shape( $name, $type, $data, $height = 0, $width = 0, $fixed = 0) {
    db_query("INSERT INTO {overlay_shape_types} (shape_name, shape_type, shape_height, shape_width, fixed_size, data) VALUES ('%s', '%s', %d, %d, %d, '%s' )", $name, $type, $height, $width, $fixed, $data);
}

function image_overlay_update_shape( $shape_id, $name, $data, $height = 0, $width = 0, $fixed = 0) {
    db_query("UPDATE {overlay_shape_types} SET shape_name = '%s', shape_height = %d, shape_width = %d, fixed_size = %d, data = '%s') WHERE shape_id = %d ", $name, $height, $width, $fixed, $data, $shape_id);
    }

//function image_overlay