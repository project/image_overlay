<?php

function image_overlay_drawing_shape(&$im, $shape) {
  $function_name = 'image_overlay_drawing_'. strtolower($shape->TypeName);
  $color = NULL;
  watchdog('imageshape', $shape->TypeName . " " . $shape->CanvasID);
  watchdog('imageshape', print_r($shape, TRUE));
  
  if ($shape->Color) {
    $color = image_overlay_drawing_parse_color($im, $shape->Color);
    $function_name($im, $shape, $shape->Absolute_Shape, $color);
  }
  else {
    $function_name($im, $shape, $shape->Absolute_Shape);
  }
  return;
}


function image_overlay_drawing_triangle(&$im, &$json, $absolute, $color) {
  $tri = image_overlay_drawing_split_points($absolute->Points, 3);
  imagesetthickness($im, 1);
  imagefilledpolygon($im, $tri, 3, $color);  
}


function image_overlay_drawing_rectangle(&$im, &$json, $absolute, $color) {
  $rect = $absolute->Rectangle;
  imagesetthickness($im, 1);
  imagefilledrectangle($im, $rect[0][0], $rect[0][1], $rect[1][0], $rect[1][1], $color);
}


function image_overlay_drawing_text(&$im, &$json, $absolute, $color){

$path = _image_overlay_load_fonts();
  watchdog("imagetext", getenv('GDFONTPATH'));
  $fontname = _image_overlay_get_data($absolute->FontID);
  $fontname .= ".ttf";
  
  
  $container = $json->Container->Rectangle;
    $top = $container[0][1];
    $bottom = $container[1][1];
    $left = $container[0][0];
    
    
    
    $x = $left;
    $y = $bottom;
    
    // getcwd should give the absolute path to the drupal root
    $font_path = getcwd() . '/' . drupal_get_path('module', 'image_overlay') .  '/fonts/';
    $trans = array("\\" => "/", "//" => "/");
  $font_path = strtr($font_path, $trans);
    watchdog("imagetext", "font is {$font_path}{$fontname}");
        imagettftext(
            $im, $absolute->Size, 0, $x, $y,
            $color, "{$font_path}{$fontname}", $absolute->Text
        );


    // calculate the size of the text
 /**   $box = imagettfbbox($absolute->Size, 0, $fontname, $absolute->Text);
    if (!$box) {
        $box = imagettfbbox($absolute->Size, 0, $fontname . '.ttf', $absolute->Text);
        if (!$box) {
      drupal_set_message(t("Is your font set correctly as '%fontname'?", array('%fontname' => $fontname)), 'error');**/
      //    imagestring($im, 5, $x, $y, $absolute->Text, $color);
//          imagefttext ($im, $absolute->Size, 0, $x, $y, $color, $fontname, $absolute->Text);
/**
      return '';
      }
    }
    $textwidth = abs($box[2]) + abs($box[0]) + 5; // sometimes text is clipped. I don't know why, so I add 5px here...
    $textheight = abs($box[1]) + abs($box[7]);

    $width = $width ? $width : $textwidth + $x;
    $height = $height ? $height : $textheight + $y;

    imagettftext($im, $size, 0, $x, $y + abs($box[5]), $color, $fontname, $absolute->Text);
  
  
  
  
  
  
  
  ////////
  /**
  $container = $json->Container->Rectangle;
  $top = $container[0][1];
  $bottom = $container[1][1];
  $midline = $top - $bottom;
  $font = 'FreeSerifA';
  


  $o = $container[0][0];
  $t = $container[1][1];
  ***/
    $pp = print_r($json, TRUE);
    watchdog("imagetext", "json is : <pre>$pp</pre>");
    $pp = print_r($absolute, TRUE);
    watchdog("imagetext", "absolute is : <pre>$pp</pre>");

}



/**
 * image_overlay_drawing_draw_arrow
 */
function image_overlay_drawing_arrow(&$im, &$json, $absolute, $color) {
  image_overlay_drawing_rectangle($im, $json, $absolute->Rectangle,  $color);
  image_overlay_drawing_triangle($im, $json, $absolute->Triangle, $color);  
}


function image_overlay_drawing_circle(&$im, &$json, $absolute, $color) {
  /** 
   * Absolute should be:
   *
   *  var ans {
   *    "Radius" : this.radius,
   *   "Center" : middle.toJSON(),
   *   "LineWidth" : this.lineWidth
    *  };
    **/
  //bool imageellipse ( resource image, int cx, int cy, int width, int height, int color )

  // height & width == diameter == (radius*2)
  $radius = $absolute->Radius;
  $diameter = ($radius) * 2;
  $center = $absolute->Center;

  $line_width = (int) $absolute->LineWidth;
  // Because of a bug in PHP GD, line thickness cannot be changed from 1 for ellipse
  // See PHP bug #25678
  if ( $line_width > 1 ) {
    // workaround:  
    imagesetthickness($im, $line_width);
    imagearc($im, $center[0], $center[1], $radius , $radius , 0, 359, $color); 
    imagearc($im, $center[0], $center[1], $radius , $radius , 359, 360, $color); 
  }
  else {
    imageellipse($im, $center[0], $center[1], $diameter, $diameter, $color);
  }
}


function image_overlay_drawing_line(&$im, &$json, $absolute, $color) {
  /**
  bool imageline ( resource $image , int $x1 , int $y1 , int $x2 , int $y2 , int $color )
	/**
    *  var ans = {
    *    "Start" : linestart.toJSON(),
    *    "End" : lineend.toJSON(),
    *    "LineWidth" : this.lineWidth,
    *    "Radius" : this.radius
    *  };
    **/
  
  $xstart = $absolute->Start[0];
  $ystart = $absolute->Start[1];
  
  $xend = $absolute->End[0];
  $yend = $absolute->End[1];
  
  $line_width = (int) $absolute->LineWidth;
  
  imagelinethick($im, $xstart, $ystart, $xend, $yend, $color, $line_width);
}


function image_overlay_drawing_freehand(&$im, &$json, $absolute, $color) {
  /**
  bool imageline ( resource $image , int $x1 , int $y1 , int $x2 , int $y2 , int $color )
	/**
  var ans = {
  		"Points" : jsonarr,
      "Counter" : this.counter,
      "LineWidth" : this.lineWidth
  	};

    **/
  
  $line_width = (int) $absolute->LineWidth;
  $points = $absolute->Points;
  for ( $i = 1; $i < count($points); $i++) {
    imageline($im, $points[$i-1][0], $points[$i-1][1], $points[$i][0], $points[$i][1], $color);
  }
}




function image_overlay_drawing_symbol(&$im, &$json, $absolute, $color) {
/**  var ans = {
    "Points" : points,
    "File" : this.file,
    "Dir" : this.dir,
    "Fixed" : this.fixed,
    "Height" : this.Height,
    "Width" : this.width,
    "ShapeID" : this.shape_id
  };***/
  
  $xleft = $absolute->Points[0][0];
  $ytop = $absolute->Points[0][1];
  
  $height = $absolute->Height;
  $width = $absolute->Width;  
  $srcheight = $absolute->NormalHeight;
  $srcwidth = $absolute->NormalWidth;
  
  $arr = explode('.', $absolute->File);
  $extension = $arr[count($arr) - 1];
  $extension = strtolower($extension);
  if (! strcmp($extension, "jpg")) {
    $extension = "jpeg";
  }
  $funct = 'imagecreatefrom' . $extension;
  $old_img = $funct($absolute->Dir . $absolute->File);
  // bool imagecopyresized  ( resource $dst_image  , resource $src_image  , int $dst_x  , int $dst_y  , int $src_x  , int $src_y  , int $dst_w  , int $dst_h  , int $src_w  , int $src_h  )
//imagecopyresized() will take an rectangular area from src_image  of width src_w  and height src_h  at position (src_x ,src_y ) and place it in a rectangular area of dst_image  of width dst_w  and height dst_h  at position (dst_x ,dst_y ).
  
  imagecopyresized($im, $old_img, $xleft, $ytop, 0, 0, $width, $height, $srcwidth, $srcheight);
//  watchdog('img',   "imagecopyresized($im, $old_img, $xleft, $ytop, 0, 0, $width, $height, $srcwidth, $srcheight)");
}



/**
 imagelinethick is stolen from the PHP manual
 **/
function imagelinethick(&$image, $x1, $y1, $x2, $y2, $color, $thick = 1) {
    /* this way it works well only for orthogonal lines
    imagesetthickness($image, $thick);
    return imageline($image, $x1, $y1, $x2, $y2, $color);
    */
    if ($thick == 1) {
        imageline($image, $x1, $y1, $x2, $y2, $color);
        return;
    }
    $t = $thick / 2 - 0.5;
    if ($x1 == $x2 || $y1 == $y2) {
        imagefilledrectangle($image, round(min($x1, $x2) - $t), round(min($y1, $y2) - $t), round(max($x1, $x2) + $t), round(max($y1, $y2) + $t), $color);
        return;
    }
    $k = ($y2 - $y1) / ($x2 - $x1); //y = kx + q
    $a = $t / sqrt(1 + pow($k, 2));
    $points = array(
        round($x1 - (1+$k)*$a), round($y1 + (1-$k)*$a),
        round($x1 - (1-$k)*$a), round($y1 - (1+$k)*$a),
        round($x2 + (1+$k)*$a), round($y2 - (1-$k)*$a),
        round($x2 + (1-$k)*$a), round($y2 + (1+$k)*$a),
    );
    imagefilledpolygon($image, $points, 4, $color);
    imagepolygon($image, $points, 4, $color);
    return;
}

