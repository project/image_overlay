function Point( int_x, int_y ) {
	this.x = int_x;
	this.y = int_y;
//	this.Set(int_x, int_y);
}
Point.prototype.GetX = function() { return this.x; }
Point.prototype.GetY = function() { return this.y; }
Point.prototype.Set = function( int_x, int_y) {
	this.x = int_x;
	this.y = int_y;
};
Point.prototype.toPrintableString = function() {
	ans = '('  + this.GetX() + ',' + this.GetY() + ')';
	return ans;
};
Point.prototype.Add = function( other_point, blank_point ) {
	var newX = this.GetX() + other_point.GetX();
	var newY = this.GetY() + other_point.GetY();
	blank_point.Set(newX, newY);
	return;
};
Point.prototype.toJSON = function () {
	return [
		this.GetX(), 
		this.GetY() 
	];
};

