
/**
 * imageBG
 *  the entire background div. Should have background-image:url(image url here)
 *
 * @param left         the absoloute left offset of the div in pixels
 * @param top          the absoloute position offset of the top in pixels
 * @param height      the height of the image (and div) in pixels
 * @param width       the width of the image (and div) in pixels
 * @param img_id      the id attribute of the div
 */
 function imageBG() {
	this.id = '';

  //The Rect that represents the whole div
  //Set the points absolutely relative to the page  
	this.imageBox = new Rect();
  this.select_id = "which-one";
    
  this.canvases = new Array();              //Each canvas represents one shape
  this.counter = 1;                                //Count of the canvases 
  this.currently_active = 0;                    //Nothing currently active	
 
 /**
	left = findInt(left);
	top = findInt(top);
	this.imageheight = findInt(height);
	this.imagewidth = findInt(width);

	this.id = img_id;

  //The Rect that represents the whole div
  //Set the points absolutely relative to the page  
	this.imageBox = new Rect();
  this.SetOffset(top, left);
    
  this.canvases = new Array();              //Each canvas represents one shape
  this.counter = 1;                                //Count of the canvases 
  this.currently_active = 0;                    //Nothing currently active	
  ***/
}

imageBG.prototype.createBG = function(top, left, height, width, img_id) {

	left = findInt(left);
	top = findInt(top);
	this.imageheight = findInt(height);
	this.imagewidth = findInt(width);

	this.id = img_id;

  //The Rect that represents the whole div
  //Set the points absolutely relative to the page  
  this.SetOffset(top, left);
    
  this.canvases = new Array();              //Each canvas represents one shape
  this.counter = 1;                                //Count of the canvases 
  this.currently_active = 0;                    //Nothing currently active	
  
};/**
imageBG.fromJSON = function( json_object, top, left ) {
  /*
  	var ans = {
		"BGObject" : {
      "ID" : this.id,
      "ImageBox" : this.imageBox.toJSON(),
      "ImageHeight" : this.imageheight,
      "ImageWidth" : this.imagewidth,
      "Counter" : this.counter,
      "Image" : image_name,
			"Type" : imagetype,
			"Canvases" : all
		}	};
**
  var jsonBG = json_object.BGObject;
  var newBG = new imageBG(top, left, jsonBG.ImageHeight, jsonBG.ImageWidth, jsonBG.ID);
  
  for( var i = 0; i < jsonBG.Canvases.length - 1; i++ ) {
    this.AddJSONCanvas(jsonBG.Canvases[i], i+1);
//    this.canvases[i].
  }

};**/

imageBG.prototype.fromJSON = function( json_object, top, left ) {
  /**
  	var ans = {
		"BGObject" : {
      "ID" : this.id,
      "ImageBox" : this.imageBox.toJSON(),
      "ImageHeight" : this.imageheight,
      "ImageWidth" : this.imagewidth,
      "Counter" : this.counter,
      "Image" : image_name,
			"Type" : imagetype,
			"Canvases" : all
		}	};
**/
  
  var jsonBG = json_object.BGObject;

 	left = findInt(left);
	top = findInt(top);
	this.imageheight = findInt(jsonBG.ImageHeight);
	this.imagewidth = findInt(jsonBG.ImageWidth);

	this.id = jsonBG.ID;

  //The Rect that represents the whole div
  //Set the points absolutely relative to the page  
  this.SetOffset(top, left);

  this.canvases = new Array();              //Each canvas represents one shape
  this.counter = 1;                                //Count of the canvases 
  this.currently_active = 0;                    //Nothing currently active	

  for( var i = 0; i < jsonBG.Canvases.length - 1; i++ ) {
    this.AddJSONCanvas(jsonBG.Canvases[i], i+1);
//    this.canvases[i].
  }
  this.counter++;

};

imageBG.prototype.AddJSONCanvas = function(json_canvas, number) {
  if (this.counter < number) { this.counter = number; }
	var new_container = bases.GetContainer() + number;
  var type = json_canvas.Type;
  var new_color = json_canvas.Color;
  
  
  if ( json_canvas.Type == ShapeType.TEXT ) {
  	var new_canvas = new_container;
    this.canvases[number] = ShapeCanvas.fromJSON(json_canvas);

    $('<span id="'+new_container+'">'+this.canvases[number].GetText()+'<br/><span/>').css({'color':new_color}).appendTo($("#"+this.id));
    $("<option>" + ShapeType.Names[json_canvas.Type] + "(" + this.canvases[number].GetText().substr(0,20) + "...)</option>")
      .val(new_container)
      .addClass(ShapeType.Group[json_canvas.Type])
      .appendTo("#" + this.select_id);
    var parent = this;
     $('#'+new_container).draggable({
        containment: 'parent',
        cursor: 'move',
        handle: $("#"+bases.GetContainer()+number),
        opacity: .7,
        stop: function() { parent.ChangeCanvas(new_container); }
      });

      return;
  }
	  if ( json_canvas.Type == ShapeType.SYMBOL ) {
  	var new_canvas = new_container;
    $('<img class="overlayshape" id="'+new_container+'" />').appendTo($("#"+this.id));
    this.canvases[number] = ShapeCanvas.fromJSON(json_canvas);
    $("<option>" + ShapeType.Names[type] + "</option>").val(new_container).addClass(ShapeType.Group[type]).appendTo("#" + this.select_id);
    var parent = this;
     $('#'+new_container).draggable({
        containment: 'parent',
        cursor: 'move',
        handle: $("#"+bases.GetContainer()+num),
        opacity: .7,
        stop: function() { parent.ChangeCanvas(new_container); }
      });
      return;
  }
  var new_canvas = bases.GetCanvas() + number;

	if ( !new_color ) { new_color = 'red'; };
  
	var copyMe = $('#' + bases.GetContainer()).clone(true).attr("id",new_container).removeClass("hidden");
  copyMe.find("canvas").attr("id", new_canvas).end();
  copyMe.appendTo($("#"+this.id));
  $("<option>" + ShapeType.Names[json_canvas.Type] + " " + number + "</option>").val(new_canvas).addClass(ShapeType.Group[json_canvas.Type]).appendTo("#" + this.select_id);

  this.canvases[number] = ShapeCanvas.fromJSON(json_canvas);
	var parent = this;
  
  var resize_with = ShapeType.GetHandles(json_canvas.Type);
  
 $('#'+new_container).draggable({
    containment: 'parent',
    cursor: 'move',
    handle: $("#"+new_canvas),
 	  cancel: '.resizeHandler',
    opacity: .7,
    stop: function() {
      parent.ChangeCanvas(new_container); 
    }
  }).resizable({
    containment: 'parent',
    handle: '.resizeHandler', 
		stop: function(e, ui) { 
			//parent.ChangeCanvas(new_container, ui) 
			parent.handleResize(e, ui, new_container) 
		}
	});

  makePretty(new_container, parent);
	
	return;

};


imageBG.prototype.ChangeCanvas = function(container_id, ui) {
	var num = findInt(container_id);
	this.canvases[num].ChangeMe( );	
};

imageBG.prototype.ChangeCanvasText = function(container_id, newtext, newfont, newsize, option_jqo) {
	var num = findInt(container_id);
	this.canvases[num].ChangeText(newtext);	
  option_jqo.text(ShapeType.Names[ShapeType.TEXT] + "(" + this.canvases[num].GetText().substr(0,20) + "...)");
};

imageBG.prototype.ChangeText = function(container_id, newtext, fw_current, option_jqo) {
  var num = findInt(container_id);
  if (this.canvases[num].GetShapeType() == ShapeType.TEXT) {
  	this.canvases[num].ChangeText(newtext);
    this.canvases[num].SetFont(fw_current.file, fw_current.size, fw_current.id, fw_current.family, fw_current.name);
  	this.canvases[num].Draw();
    option_jqo.text(ShapeType.Names[ShapeType.TEXT] + "(" + this.canvases[num].GetText().substr(0,20) + "...)");
  }
};
imageBG.prototype.GetFontID = function(container_id) {
  var num = findInt(container_id);
  if (this.canvases[num].GetShapeType() == ShapeType.TEXT) {
    return this.canvases[num].GetFontID();
  }
  return -1;
};
imageBG.prototype.GetFontSize = function(container_id) {
  var num = findInt(container_id);
  if (this.canvases[num].GetShapeType() == ShapeType.TEXT) {
    return this.canvases[num].GetFontSize();
  }
  return -1;
};

imageBG.prototype.SetSymbol = function(container_id, dir, sw_current, option_jqo){
  var num = findInt(container_id);
  if (this.canvases[num].GetShapeType() == ShapeType.SYMBOL) {
    //Symbol.prototype.SetSymbol = function( new_filename, new_height, new_width, new_fixed, new_shape_id ) {
    this.canvases[num].SetSymbol(sw_current.file, sw_current.height, sw_current.width, sw_current.fixed, sw_current.id, dir);
  	this.canvases[num].Draw();
    option_jqo.text(ShapeType.Names[ShapeType.SYMBOL] + num);
    
  }


};

imageBG.prototype.SetOffset = function(top, left) {
  left = findInt(left);
  top= findInt(top);
  height = findInt(this.imageheight);
  width = findInt(this.imagewidth);
  
	this.imageBox.SetPoints(left, left + width, top, top + height);
	this.maxW = this.imageBox.GetWidth()/2;                                           //max width
	this.maxB = this.imageBox.GetBottomY() - this.imageBox.GetTopY(); //max down
	this.maxR = this.imageBox.GetRightX() - this.imageBox.GetLeftX();    //max right
	this.maxH= this.imageBox.GetHeight();                                              //max height
	this.minL= this.imageBox.GetLeftX();                                                  //min left
};

imageBG.prototype.ChangeOffset = function(oldoffset, newoffset) {
  var oldleft = oldoffset.left;
  var oldtop = oldoffset.top;
  
  var newleft = newoffset.left;
  var newtop = newoffset.top;
  
  var topmove = newtop - oldtop;
  var leftmove = newleft - oldleft;
  
	this.imageBox.SetPoints(newleft, newleft + this.imagewidth, newtop, newtop + this.imageheight);
  
  
	for ( num = 1; num < this.canvases.length; num++ ) {
    this.canvases[num].MoveMe(leftmove, topmove);
  }


};


imageBG.prototype.ChangeLine= function ( num_or_id, int_width ) {  
	var num = -1;
	if (num_or_id) {
		var num = findInt(num_or_id);
    this.canvases[num].SetLine(int_width);

	}
};

imageBG.prototype.DisableResize= function ( num_or_id ) {
		var type = "resizable";
		var num = -1;
	if (num_or_id) {
		var num = parseInt(num_or_id);
	}
	this.Disable(type, num);
};
imageBG.prototype.DisableDrag= function ( num_or_id ) {
		var type = "draggable";
		var num = -1;
	if (num_or_id) {
		var num = parseInt(num_or_id);
	}
	this.Disable(type, num);
};

imageBG.prototype.EnableDrag= function ( num_or_id ) {
		var type = "draggable";
		var num = -1;
	if (num_or_id) {
		var num = parseInt(num_or_id);
	}
	this.Enable(type, num);
};
imageBG.prototype.EnableDrag= function ( num_or_id ) {
		var type = "resizable";
		var num = -1;
	if (num_or_id) {
		var num = parseInt(num_or_id);
	}
	this.Enable(type, num);
};

imageBG.prototype.Disable = function ( type, num ) {
	var base = bases.GetContainer();
	if ( num >= 0 ) {
		$("#"+base+num).disable(type);		
	} else {
		for ( num = 1; num < this.canvases.length; num++ ) {
			$("#"+base+num).disable();		
		}
	}
};
imageBG.prototype.Enable = function ( type, num ) {
	var base = bases.GetContainer();
	if ( num >= 0 ) {
		$("#"+base+num).enable(type);		
	} else {
		for ( num = 1; num < this.canvases.length; num++ ) {
			$.enable(base+num, type);		
		}
	}
};
imageBG.prototype.Freeze = function ( ) {
	var base = bases.GetContainer();
  var num;
  for ( num = 1; num < this.canvases.length; num++ ) {
    var container_name = this.canvases[num].container_id;
    var container = $("#"+container_name);
    container.draggable("disable");		//draggableDisable
    container.resizable("disable");
    removePretty(container_name, this);
  }
};
imageBG.prototype.Defrost = function ( ) {
	var base = bases.GetContainer();
  var num;
  for ( num = 1; num < this.canvases.length; num++ ) {
    var container_name = this.canvases[num].container_id;
    var container = $("#"+container_name);
    container.draggable("enable");		//draggableDisable
    container.resizable("enable");
    makePretty(container_name, this);
  }
};


imageBG.prototype.AddCanvas = function( type, left, right, top, bottom, new_color ) {
	var num = this.counter++;
	var new_container = bases.GetContainer() + num;
  
  if ( type == ShapeType.TEXT ) {
  	var new_canvas = new_container;
    $('<span id="'+new_container+'">'+ShapeText.GetDefault()+'</span><br/>').css({'color':new_color}).addClass("overlayshape").appendTo($("#"+this.id));
    this.canvases[num] = ShapeCanvas.newShapeCanvas(new_color, type, new_container, new_canvas, left, right, top, bottom);
  	this.canvases[num].ChangeText(ShapeText.GetDefault());
    $("<option>" + ShapeType.Names[type] + "(" + this.canvases[num].GetText().substr(0,20) + "...)</option>").val(new_container).addClass(ShapeType.Group[type]).appendTo("#" + this.select_id);
    var parent = this;
     $('#'+new_container).draggable({
        containment: 'parent',
        cursor: 'move',
        handle: $("#"+bases.GetContainer()+num),
        opacity: .7,
        stop: function() { parent.ChangeCanvas(new_container); }
      });

      this.canvases[num].Draw();
      return num;
  }
 
  if ( type == ShapeType.SYMBOL) {
  	var new_canvas = new_container;
    $('<img class="overlayshape" id="'+new_container+'" />').appendTo($("#"+this.id));
    this.canvases[num] = ShapeCanvas.newShapeCanvas(new_color, type, new_container, new_canvas, left, right, top, bottom);
    $("<option>" + ShapeType.Names[type] + "</option>").val(new_container).addClass(ShapeType.Group[type]).appendTo("#" + this.select_id);
    var parent = this;
     $('#'+new_container).draggable({
        containment: 'parent',
        cursor: 'move',
        handle: $("#"+bases.GetContainer()+num),
        opacity: .7,
        stop: function() { parent.ChangeCanvas(new_container); }
      });/*.resizable({
    containment: 'parent',
    handle: '.resizeHandler', 
		stop: function(e, ui) { 
			//parent.ChangeCanvas(new_container, ui) 
			parent.handleResize(e, ui, new_container) 
		}
    
	});*/

      this.canvases[num].Draw();
      return num;
  }
 
 
  var new_canvas = bases.GetCanvas() + num;
  var parent = this;

	var copyMe = $('#' + bases.GetContainer()).clone(true).attr("id",new_container).removeClass("hidden");
  copyMe.find("canvas").attr("id", new_canvas).addClass('outlined').end();
  copyMe.appendTo($("#"+this.id));
  $("<option>" + ShapeType.Names[type] + " " + num + "</option>").val(new_canvas).addClass(ShapeType.Group[type]).addClass("overlayshape").appendTo("#" + this.select_id);
  copyMe.attr("height", (bottom - 10) + "px");
  copyMe.attr("width", (right - 10) + "px");

	this.canvases[num] = ShapeCanvas.newShapeCanvas(new_color, type, new_container, new_canvas, left, right, top, bottom);
  var mycanvas = this.canvases[num];
  var offset = $("#"+this.id).offset();
  
	if (type == ShapeType.FREEHAND) {
    $('#'+new_canvas).one( 'mousedown', function(e) {
        var leftoff = offset.left;
        var topoff = offset.top;
      $(this).mousemove(function(e){
      	var x = e.pageX - leftoff;
        var y = e.pageY - topoff;
        mycanvas.AddPoint(x, y);
//        mycanvas.SetNextPoint(x, y);
      });
      $(this).one('mouseup', function(e){
        $('#'+new_container).draggable({
          containment: 'parent',
          cursor: 'move',
          handle: $("#"+bases.GetCanvas()+num),
       	  cancel: '.resizeHandler',
          opacity: .7,
          stop: function() {
            parent.ChangeCanvas(new_container); 
          }
        });
        $(this).unbind('mousemove');
        $(this).removeClass('outlined');
        mycanvas.AutoResize();

      });
    }); 

   return num;
  }
      $('#'+new_canvas).removeClass('outlined');

  
  var resize_with = ShapeType.GetHandles(type);
  
 $('#'+new_container).draggable({
    containment: 'parent',
    cursor: 'move',
    handle: $("#"+bases.GetCanvas()+num),
 	  cancel: '.resizeHandler',
    opacity: .7,
    stop: function() {
      parent.ChangeCanvas(new_container); 
    }
  }).resizable({
    containment: 'parent',
    handle: '.resizeHandler', 
		stop: function(e, ui) { 
			//parent.ChangeCanvas(new_container, ui) 
			parent.handleResize(e, ui, new_container) 
		}
    
	});
	/*
	All callbacks (start,stop,drag) receive two arguments: The original browser event and a prepared ui object. The ui object has the following fields:

    * ui.options - options used to initialize the resizable
    * ui.axis - which handle was dragged, as a string matching the 'handles' option
    * ui.instance - the UI Resizables instance containing internal data. The most useful fields in here may be:
          o ui.instance.element - JQuery object containing the original Resizable element
          o ui.instance.helper - JQuery object with the proxy object displaying the resize
          o ui.instance.size - new size of the element, as { width, height } object*/

  makePretty(new_container, parent);
	
	return num;
}
imageBG.prototype.GetText = function( container_id ) {
  var ans = $("#"+container_id);
  if ( ans.is("span") ) {
    return ans.text();
  }
  return false;
};
imageBG.prototype.handleResize = function (e, ui, name){
	var num = findInt(name);
  if (e.type == 'resizestop') {
    
  }
	/*var new_top = 
	var new_left = 
	var new_width = 
	var new_height = 
	originalSize (height, width)
size(height, width)
position(left, top)
containerOffset(left, top)
*/
  if (this.canvases[num].GetTypeName() == ShapeType.Names[ShapeType.SYMBOL]) {
    
  }
	this.canvases[num].ChangeMe( );	
};


function makePretty(new_container, parent) {
  $("#"+new_container).addClass("resizeMe");  
  $("#"+new_container).bind('mouseover', {
  bg_id : parent.id,
  container_id : new_container
  }, activateCanvas);
/**  $("#"+new_container).bind('mouseout', {
  bg_id : parent.id,
  container_id : new_container
  }, deactivateCanvas);**/

}

function removePretty(new_container, parent) {
  $("#"+new_container).removeClass("resizeMe");  
  $("#"+new_container).unbind('mouseover');
  $("#"+new_container).children();
}

//imageBG.prototype.makeResizable = function( num ) {};
imageBG.prototype.ChangeCanvasColor = function(id_or_num, new_color) {
	var num = findInt(id_or_num);
	this.canvases[num].SetColor(new_color);
	this.Draw(num);
};
imageBG.prototype.Draw = function( id_or_num ) {		// optional parameter
	if (id_or_num) {
		var num = parseInt(id_or_num);
		this.canvases[num].Draw();
	} else {
		for ( num = 1; num < this.canvases.length; num++ ) {
			this.canvases[num].Draw();
		}
	}
};
imageBG.prototype.toJSON = function( image_uri ) {
	var image = $("#"+this.id);
  var last_slash = image_uri.lastIndexOf('/');
	var image_name = image_uri.substring(last_slash+1);
	var image_path = image_uri.substring(0, last_slash);
	var i = image_name.lastIndexOf('.');
	var imagetype = image_name.substring(i+1);
	var all = new Array();

	for(i = 1; i < this.canvases.length; i++) {
    all[i] = this.canvases[i].toJSON();
	}
  all[i] = new Object();
  
	
	var ans = {
		"BGObject" : {
      "ID" : this.id,
      "ImagePath" : image_path,
      "ImageBox" : this.imageBox.toJSON(),
      "ImageHeight" : this.imageheight,
      "ImageWidth" : this.imagewidth,
      "Counter" : this.counter,
      "Image" : image_name,
			"Type" : imagetype,
			"Canvases" : all
		}
	};
	return ans;
};
imageBG.prototype.HighlightCanvas = function (just_id) {
	var num = parseInt(just_id);
	$("#"+just_id).show('pulsate',{times: 5 });
//	this.canvases[num].addClass("highlighted");
};
/*imageBG.prototype.Rotate= function ( num_or_id ) {
	var num = parseInt(num_or_id);
	this.canvases[num].Rotate( 45 );
	this.canvases[num].Draw();
	return;
};*/

var activateCanvas = function( event ) {
  $(this).find(".resizeHandler ").show();
  var x = $("#"+event.data.container_id);
  $("#"+event.data.bg_id).one('click', function(){
    $("#"+event.data.container_id).find(".resizeHandler")
    .each(function( ){
      $(this).hide();
    });
  });
};

function StringIDBasics(resize, canvas) {
	this.resize = resize;
	this.canvas = canvas;
}
StringIDBasics.prototype.GetContainer = function() { return this.resize; };
StringIDBasics.prototype.GetCanvas = function() { return this.canvas; };


var bases = new StringIDBasics("resizeMe_", "canvas_");

