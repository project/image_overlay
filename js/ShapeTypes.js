/**
* ShapeType
  *   A way of representing the different types of shapes. 
  *
  *   Create a new Object of TYPE:
  *        ShapeType.Classes[ ShapeType.{TYPE} ] 
  *   
  *    Retrieve the human-readable name of a shape:
  *       ShapeType.Names[ ShapeType.{TYPE} ]
  *
  *    Get the resize-handlers of a shape:
  *        ShapeType.GetHandles( ShapeType.{TYPE} )
  *
  *   
  */
function ShapeType( num ) {	this.type = num; }
/**ShapeType = new Object();**/
ShapeType.RECT = 0;
ShapeType.ARROW = 1;
ShapeType.CIRCLE = 2;
ShapeType.TRIANGLE = 3;
ShapeType.LINE = 4;
ShapeType.TEXT = 5;
ShapeType.FREEHAND = 6;
ShapeType.SYMBOL = 7;
ShapeType.Classes = new Array();
ShapeType.Classes[ShapeType.RECT] = function ( ) { return new Rect(); };
ShapeType.Classes[ShapeType.ARROW] = function ( ) { return new Arrow(); };
ShapeType.Classes[ShapeType.CIRCLE] = function ( ) { return new Circle(); };
ShapeType.Classes[ShapeType.TRIANGLE] = function ( ) { return new Triangle(); };
ShapeType.Classes[ShapeType.LINE] = function ( ) { return new Line(); };
ShapeType.Classes[ShapeType.TEXT] = function ( ) { return new ShapeText(); };
ShapeType.Classes[ShapeType.FREEHAND] = function ( ) { return new Freehand(); };
ShapeType.Classes[ShapeType.SYMBOL] = function ( ) { return new Symbol(); };

ShapeType.fromJSON = new Array();
ShapeType.fromJSON[ShapeType.RECT] = function (json_in_container) { return Rect.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.ARROW] = function (json_in_container) { return Arrow.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.CIRCLE] = function (json_in_container) { return Circle.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.TRIANGLE] = function (json_in_container) { return Triangle.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.LINE] = function (json_in_container) { return Line.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.TEXT] = function (json_in_container) { return ShapeText.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.FREEHAND] = function (json_in_container) { return Freehand.fromJSON(json_in_container); };
ShapeType.fromJSON[ShapeType.SYMBOL] = function (json_in_container) { return Symbol.fromJSON(json_in_container); };

ShapeType.Names = new Array();
ShapeType.Names[ShapeType.RECT] = "Rectangle";
ShapeType.Names[ShapeType.ARROW] = "Arrow"; 
ShapeType.Names[ShapeType.CIRCLE] = "Circle"; 
ShapeType.Names[ShapeType.TRIANGLE] = "Triangle";
ShapeType.Names[ShapeType.LINE] = "Line";
ShapeType.Names[ShapeType.TEXT] = "Text";
ShapeType.Names[ShapeType.FREEHAND] = "Freehand";
ShapeType.Names[ShapeType.SYMBOL] = "Symbol";
ShapeType.Group = new Array();
ShapeType.Group[ShapeType.RECT] = "polygon";
ShapeType.Group[ShapeType.ARROW] = "polygon"; 
ShapeType.Group[ShapeType.CIRCLE] = "polygon";
ShapeType.Group[ShapeType.TRIANGLE] = "polygon";
ShapeType.Group[ShapeType.LINE] = "Line";
ShapeType.Group[ShapeType.TEXT] = "Text";
ShapeType.Group[ShapeType.FREEHAND] = "Freehand";
ShapeType.Group[ShapeType.FREEHAND] = "Symbol";


ShapeType.GetHandles = function( num ) {

/*var number = parseInt(num);

  switch(number) {
/*    case ShapeType.CIRCLE:
      return {
        se: '.resizeSE'
      };
      break;
    default:*/
      return {
        e: '.resizeE',
        s: '.resizeS',
        se: '.resizeSE'
      };
    /*  break;
  }*/
};

/**
  * ShapeType.GetList
  *     Returns an array with all the human-readable names of shape TYPEs
  */
ShapeType.GetList = function( ) {
  var ans = new Array( );
  for( num in ShapeType.Names ) {
    ans[num] = ShapeType.Names[num];
  }
  return ans;
};

ShapeType.GetPoint = function ( container, add_point ) {
  if ( container != false ) {
    return container.GetPoint(1);
  }
  else if (add_point != false) {
    return add_point;
  }
  else {
    return new Point(0,0);
  }
}



/*
/**************************************************************************************
**			Following are the actual shape classes. Note that Rect has special qualities, as it is used for the entire canvas 
**	as well as the div.
**************************************************************************************/


//////////////////////////////// Rect ///////////////////////
function Rect() {
	this.TopLeft  = new Point();
	this.TopRight = new Point();
	this.BottomLeft = new Point();
	this.BottomRight = new Point();
}

Rect.GetBottomSpace = function( ) { return 0; };
Rect.GetRightPadding = function( ){ return 0; };
Rect.prototype.SetPoints = function(left_x, right_x, upper_y, lower_y) {
  left_x = parseInt(left_x);
  right_x = parseInt(right_x);
  upper_y = parseInt(upper_y);
  lower_y = parseInt(lower_y);
  
	this.TopLeft.Set(left_x, upper_y);
	this.TopRight.Set(right_x, upper_y);
	this.BottomLeft.Set(left_x, lower_y);
	this.BottomRight.Set(right_x, lower_y);
};
Rect.prototype.GetLeftX = function() { return this.TopLeft.GetX(); };
Rect.prototype.GetRightX = function() { return this.TopRight.GetX(); };
Rect.prototype.GetTopY = function() { return this.TopLeft.GetY(); };
Rect.prototype.GetBottomY = function() { return this.BottomLeft.GetY(); };
Rect.prototype.GetWidth = function() { return this.GetRightX() - this.GetLeftX(); };
Rect.prototype.GetHeight = function() { return this.GetBottomY() - this.GetTopY(); };
Rect.prototype.GetArea = function() { return this.GetHeight() * this.GetWidth(); };
Rect.prototype.GetPoint = function ( int_which ) { //1 = upperleft, 2 = upper right, 3 = lower left, 4 = lower right
	var ans = 0;
	switch ( int_which ) {
		case 1:
			ans = this.TopLeft;
			break;
		case 2:
			ans = this.TopRight;
			break;
		case 3:
			ans = this.BottomLeft;
			break;
		case 4:
			ans = this.BottomRight;
			break;	
	}
		return ans;
};
Rect.prototype.Calculate = function(canvas, x_padding, y_padding) {
  var height = findInt(canvas.css("height")) - (2 * y_padding); 
  var width =  findInt(canvas.css("width")) - (2 * x_padding); 
	if ( height < 0 ) { height *= (-1); }
	
//	var y_top =findInt(canvas.css("top"));
	var y_top = 0 + y_padding;
	if( y_top < 0 ) { y_top *= (-1);};
	var y_bottom = y_top + height;

//	var x_left = findInt(canvas.css("left"));
	var x_left = 0 + x_padding;
	var x_right = x_left + width - x_padding;
  
  
	
	this.SetPoints(x_left, x_right, y_top, y_bottom);
};
Rect.prototype.Draw = function(id,color) { 
  var canvas = document.getElementById(id);
  // Make sure we don't execute when canvas isn't supported
  if (canvas.getContext) {
    // use getContext to use the canvas for drawing
    var ctx = canvas.getContext('2d');
    ctx.fillStyle = color;
    ctx.lineWidth = 1;
    ctx.fillRect(this.GetLeftX(), this.GetTopY(), this.GetWidth(), this.GetHeight());
	}
//fillRect(x,y,width,height) : Draws a filled rectangle
};
Rect.prototype.GetDescription = function (color) { return color; };
Rect.prototype.toJSON = function( container, color, add_point, bool_do_math) { 
  if (bool_do_math) {
  	var starter = Point();
    if (container)
      starter = ShapeType.GetPoint(container, add_point );
    else
    starter = add_point;

    var newUpperLeft = new Point( 0, 0 );
  	var newLowerRight = new Point( 0, 0 );

  	this.TopLeft.Add(starter, newUpperLeft);
  	this.BottomRight.Add(starter, newLowerRight);
    
    var points = [
      newUpperLeft.toJSON(), 
      newLowerRight.toJSON() 
    ];

    var ans = {
      "Rectangle" : points
    };
    return ans;
  }
  else {    
    var points = [
      this.TopLeft.toJSON(), 
      this.BottomRight.toJSON() 
    ];

    var ans = {
      "Rectangle" : points
    };
    return ans;
  }
};
Rect.fromJSON = function(inside_container) {
  var newshape = new Rect();
  var oldrect = inside_container.Rectangle;
  /**
    top x = oldrect[0][0], top_y = oldrect[0][1]
    bottom x = oldrect[1][0], bottom y = oldrect[1][1]
  **/
  newshape.SetPoints(oldrect[0][0], oldrect[1][0], oldrect[0][1], oldrect[1][1]);
  return newshape;
};

//////////////////////// Triangle //////////////////////////

function Triangle( ) {
	this.points = new Array();
	this.points[0] = new Point();
	this.points[1] = new Point();
	this.points[2] = new Point();
}
Triangle.fromJSON = function(inside_container) {
  var oldtri = inside_container.Points;
  var newshape = new Triangle();
  
  for (i = 0; i < 3; i++) {
    newshape.SetPoint(i, oldtri[i][0], oldtri[i][1]);
  }
  return newshape;
};

Triangle.prototype.SetPoint = function( int_point_num, int_x, int_y ) {
	this.points[int_point_num].Set(int_x, int_y);
};
Triangle.prototype.Draw = function( id, color ) {
  var canvas = document.getElementById(id);
  if (canvas.getContext) {						// Make sure we don't execute when canvas isn't supported
    var ctx = canvas.getContext('2d');
		ctx.lineWidth = 1;
    // Filled triangle
		ctx.fillStyle = color;

    ctx.beginPath();
    ctx.moveTo(this.points[0].GetX(), this.points[0].GetY());
    ctx.lineTo(this.points[1].GetX(), this.points[1].GetY());
    ctx.lineTo(this.points[2].GetX(), this.points[2].GetY());
		ctx.closePath();
    ctx.fill();
    
  }
};
Triangle.prototype.toPrintableString = function( ) {
	ans = "[";
	var i;
	for ( i = 0; i < 3; i++ ) {
		ans += this.points[i].toPrintableString();
	}
	ans += "]";
	return ans; 
 };
Triangle.prototype.GetPoints = function() {
  var all_points = new Array();
  
  all_points[0] =  new Point();
  all_points[1] = new Point();
  all_points[2] = new Point();
  
  all_points[0].Set(this.points[0].GetX(), this.points[0].GetY());
  all_points[1].Set(this.points[1].GetX(), this.points[1].GetY());
  all_points[2].Set(this.points[2].GetX(), this.points[2].GetY());
  
  
	return all_points;
};
Triangle.prototype.Calculate = function(canvas, x_padding, y_padding ) {
  var total_height = findInt(canvas.css("height")); 
  var total_width =  findInt(canvas.css("width")); 
  
  top_y = y_padding;
  
	var tip_y = total_height + y_padding;
	var tip_x =  total_width / 2 ;

	var left = x_padding;
	var right = total_width - x_padding;
	
	this.SetPoint(0, left, top_y);
	this.SetPoint(1, right, top_y);
	this.SetPoint(2, tip_x, tip_y);	
};
Triangle.prototype.toJSON = function( container, color, add_point, bool_do_math) { 
  var tri = this.GetPoints();
  var jsontri = new Array();
	var i;
  
  if (bool_do_math) {
  	var starter = ShapeType.GetPoint(container, add_point);
    	
  	for(i = 0; i < tri.length; i++) {
  		tri[i].Add(starter, tri[i]);
  		jsontri[i] = tri[i].toJSON();		
  	}
  	
  	var ans = {
  		"Points" : jsontri
  	};
  	return ans;
  }
  else {
  	for(i = 0; i < tri.length; i++) {
  		jsontri[i] = tri[i].toJSON();		
  	}
  	
  	var ans = {
  		"Points" : jsontri
  	};
    return ans;
  }
}
Triangle.prototype.GetDescription = function ( color ) { return color; };

//////////////////////////  Arrow /////////////////////////

function Arrow ( ) { 
	this.rectanglePart = new Rect();
	this.trianglePart = new Triangle();
}
Arrow.fromJSON = function(inside_container) {
  var oldtri = inside_container.Triangle;
  var oldrect = inside_container.Rectangle;
  var newshape = new Arrow();

  newshape.rectanglePart = Rect.fromJSON(oldrect);
  newshape.trianglePart = Triangle.fromJSON(oldtri);
  return newshape;
};

Arrow.GetTriXPadding = function() { return 5; };
Arrow.GetRectangleHeightFraction = function( ) { return (7/10); };
Arrow.GetTriangleHeightFraction = function( ) { return (1 - Arrow.GetRectangleHeightFraction()); };
Arrow.GetYPadding = function() { return 5; };
Arrow.GetRectXPadding = function() { return (Arrow.GetTriXPadding() + 20); };

//Arrow.prototype.ChangeDirection( int_face ) {}
  
Arrow.prototype.toJSON = function( container, color, add_point, bool_do_math ) {
	var starter = ShapeType.GetPoint(container, add_point );
	
	var ans = {
    "Triangle": this.trianglePart.toJSON(false, color, starter, bool_do_math),
    "Rectangle": this.rectanglePart.toJSON(false, color, starter, bool_do_math)
	};
	return ans;
};
Arrow.prototype.Draw = function(canvas_id, color ) {
	this.rectanglePart.Draw(canvas_id, color);
	this.trianglePart.Draw(canvas_id, color);
};	
Arrow.prototype.Calculate = function ( canvas, x_padding, y_padding ) { 
  var height = findInt(canvas.css("height")) - (2 * Arrow.GetYPadding()); 
  var width =  findInt(canvas.css("width")); 


	this.CalculateRect( canvas, Arrow.GetRectXPadding(), Arrow.GetYPadding(), height, width );
	this.CalculateTriangle( canvas, Arrow.GetTriXPadding(), Arrow.GetYPadding(), height, width );
};
Arrow.prototype.GetDescription = function ( color ) { return ''; };

// these are "private" (pretend)
Arrow.prototype.CalculateRect	= function(canvas, x_padding, y_padding, total_height, total_width) {
  var width =  total_width - (2 * x_padding); 
  var height = total_height * Arrow.GetRectangleHeightFraction();
	if ( height < 0 ) { height *= (-1); }

	this.rectanglePart.SetPoints(x_padding, x_padding + width, y_padding, y_padding + height);
};
Arrow.prototype.CalculateTriangle = function( canvas, x_padding, y_padding, total_height, total_width ) {
  var width =  total_width - (2 * x_padding); 
  var height = total_height - this.rectanglePart.GetHeight();
	if ( height < 0 ) { height *= (-1); }

  // find the bottom of the rectangle and add the height of the triangle (and the padding) to get the y part of tip
	var tip_y = this.rectanglePart.GetBottomY() + height;
  // find the middle of the rectangle by adding 1/2 the total width to the edge of the rectangle to get the x part of the tip
	var tip_x = this.rectanglePart.GetLeftX() + ( this.rectanglePart.GetWidth() / 2 );
	
  var left_x = Arrow.GetTriXPadding();
  var right_x = total_width - Arrow.GetTriXPadding();

  this.trianglePart.SetPoint(0, left_x, this.rectanglePart.GetBottomY());
  this.trianglePart.SetPoint(1, right_x, this.rectanglePart.GetBottomY());
  this.trianglePart.SetPoint(2, tip_x, tip_y);	
};
Arrow.prototype.Rotate = function ( num ) {
	var canvas = document.getElementById(this.id);	// Make sure we don't execute when canvas isn't supported
	if (canvas.getContext) {  // use getContext to use the canvas for drawing
	var ctx = canvas.getContext('2d');
		ctx.rotate(num);
	}
};


///////////////////////// Circle /////////////////////////
function Circle() {
	this.center = new Point();
	this.lineWidth = 1;  // Because of a bug in GD, this should NOT be changed. See drawing_shapes.inc for details.
	this.radius = 3;
}
Circle.fromJSON = function (inside_container) {
  var newshape = new Circle();
  newshape.SetCenter(inside_container.Center[0], inside_container.Center[1]);
  newshape.SetLineWidth(inside_container.LineWidth);
  newshape.SetRadius(inside_container.Radius);
  return newshape;
};

Circle.prototype.SetCenter = function ( center_x, center_y ) { this.center.Set(center_x, center_y); };
Circle.prototype.SetLineWidth = function ( width ) { this.lineWidth = findInt(width); };
Circle.prototype.SetRadius = function ( radius ) { this.radius = radius; };
Circle.prototype.Calculate = function ( canvas, x_padding, y_padding ) { 
  var height = findInt(canvas.css("height")) - (2 * y_padding); 
  var width =  findInt(canvas.css("width")) - (2 * x_padding); 
	if ( height < 0 ) { height *= (-1); }
  
	var halfheight = height / 2;
	var halfwidth = width / 2;

	this.SetCenter( (x_padding + halfwidth), (y_padding + halfheight) );
	this.SetRadius( min(halfheight, halfwidth) );
}
Circle.prototype.Draw = function (id, color) {
  var canvas = document.getElementById(id);
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');
		ctx.lineWidth = this.lineWidth;
    ctx.strokeStyle = color;
    /** CHANGED FROM beloiw to next line**/
		//ctx.arc(this.center.GetX(), this.center.GetY(), this.radius, 0, ( Math.PI * 2 ), true);
		ctx.arc(this.center.GetX(), this.center.GetY(), this.radius, 0, ( Math.PI * 2 ), true);
		ctx.stroke();
  }
};
Circle.prototype.toJSON = function( container, color, add_point, bool_do_math ) { 
  if (bool_do_math) {
  	var starter = ShapeType.GetPoint(container, add_point );
    
    var middle = new Point(0,0);
    this.center.Add(starter, middle);
    
    var ans = {
  		"Radius" : this.radius,
      "Center" : middle.toJSON(),
      "LineWidth" : this.lineWidth
    };
  	return ans;
  }
  else {
    var ans = {
      "Center" : this.center.toJSON(),
      "LineWidth" : this.lineWidth,
      "Radius" : this.radius
    };
    return ans;
  }
};
Circle.prototype.GetDescription = function ( ) { return ''; };

  

////////////////////// Line //////////////////////////////
function Line() {
  this.start = new Point();
  this.end = new Point();
  this.padding = 2;
  this.lineWidth = 1;
}
Line.fromJSON = function(inside_container) {
  var newshape = new Line();
  newshape.SetPoints(inside_container.Start[0], inside_container.End[0], inside_container.Start[1], inside_container.End[1]);
  return newshape;
};
Line.prototype.SetPoints = function ( start_x, end_x, start_y, end_y ) {
  this.start.Set(0,0);
  //this.start.Set(start_x, start_y);
  //this.end.Set(end_x, end_y);
  this.end.Set(start_x + end_x, start_y + end_y);
};
Line.prototype.Calculate = function (canvas, x_padding, y_padding ) {
  var height = findInt(canvas.css("height")); 
  var width =  findInt(canvas.css("width")); 
	if ( height < 0 ) { height *= (-1); }
	
	var y_top = findInt(canvas.css("top"));
	if( y_top < 0 ) { y_top *= (-1);};
	var y_bottom = y_top + height;

	var x_left = findInt(canvas.css("left"));
	var x_right = x_left + width;
	
	this.SetPoints(x_left, x_right, y_top, y_bottom);
	this.SetPoints(0, width, 0, height);
};
Line.prototype.Draw = function( id, color ) {
  var canvas = document.getElementById(id);
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');
		ctx.lineWidth = this.lineWidth;
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(this.start.GetX(), this.start.GetY());
    ctx.lineTo(this.end.GetX(), this.end.GetY());
		ctx.stroke();
  }
};
Line.prototype.toJSON = function ( container, color, add_point, bool_do_math ) {
  if (bool_do_math) {
    var starter = ShapeType.GetPoint(container, add_point );

    var linestart = new Point(0,0);
    var lineend = new Point(0,0);
    this.start.Add(starter, linestart);
    this.end.Add(starter, lineend);
  	
    var ans = {
      "Start" : linestart.toJSON(),
      "End" : lineend.toJSON(),
      "LineWidth" : this.lineWidth,
      "Radius" : this.radius
    };
    return ans;
  }
  else {
    var ans = {
  		"Start" : this.start.toJSON(),
      "End" : this.end.toJSON(),
      "LineWidth" : this.lineWidth
    };    
    return ans;
  }
};
Line.prototype.GetDescription = function ( ) { return ''; };



////////////////////// Freehand //////////////////////////////
function Freehand() {
  this.points = new Array();
  this.counter = 0;
  this.LineWidth = 1;
  this.padding = 2;
  this.lineWidth = 1;
  this.TopLeft  = new Point();
	this.BottomRight = new Point();

}
Freehand.fromJSON = function(inside_container) {
  var newshape = new Freehand();
  var i = 0;
  for( i = 0; i < inside_container.Counter; i++ ) {
    newshape.SetNextPoint(inside_container.Points[i][0], inside_container.Points[i][1]);
  }
  return newshape;
};
Freehand.prototype.SetNextPoint = function( int_x, int_y ) {
  this.points[this.counter] = new Point();
  this.points[this.counter].Set(int_x, int_y);
	this.counter++;
};
Freehand.prototype.AddPoint = function(int_x, int_y, id, color) {
  var canvas = document.getElementById(id);
  var i = 0;
  this.SetNextPoint(int_x, int_y);
  
  if ( this.counter < 2 ) { return; }
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');
    ctx.save();
    ctx.strokeStyle = color;
      ctx.beginPath();
      ctx.moveTo(this.points[this.counter-2].GetX(), this.points[this.counter-2].GetY());
      ctx.lineTo(this.points[this.counter-1].GetX(), this.points[this.counter-1].GetY());
      ctx.stroke();
      ctx.closePath();
      ctx.restore();
  }
};
Freehand.prototype.Calculate = function (canvas, x_padding, y_padding ) {
  var height = findInt(canvas.css("height")); 
  var width =  findInt(canvas.css("width")); 
	if ( height < 0 ) { height *= (-1); }
	
	var y_top = findInt(canvas.css("top"));
	if( y_top < 0 ) { y_top *= (-1);};
	var y_bottom = y_top + height;

	var x_left = findInt(canvas.css("left"));
	var x_right = x_left + width;
	
	this.SetPoints(x_left, x_right, y_top, y_bottom);
	this.SetPoints(0, width, 0, height);
};
Freehand.prototype.ReCalculate = function (canvas, height, width, y_top, x_left ) {
	if ( height < 0 ) { height *= (-1); }	
	if( y_top < 0 ) { y_top *= (-1);};

	var y_bottom = y_top + height;
	var x_right = x_left + width;
	
  canvas.css("left", x_left);
  canvas.css("top", y_top);
  var css = {
    "left" : x_left,
    "top" : y_top,
    "height" : height,
    "width" : width
  };
  
  
  
	this.SetPoints(x_left, x_right, y_top, y_bottom);
	this.SetPoints(0, width, 0, height);
  return css;
};
Freehand.prototype.Draw = function( id, color ) {
  if (!this.counter) {return;}

  var canvas = document.getElementById(id);
  var i = 0;
  if (canvas.getContext){
    var ctx = canvas.getContext('2d');
		ctx.lineWidth = this.lineWidth;
    ctx.strokeStyle = color;
    ctx.beginPath();
    ctx.moveTo(this.points[0].GetX(), this.points[0].GetY());
    for( i = 1; i < this.counter; i++) {
      ctx.lineTo(this.points[i].GetX(), this.points[i].GetY());
    }
		ctx.stroke();
  }
};
Freehand.prototype.SetPoints = function(left_x, right_x, upper_y, lower_y) {
  left_x = parseInt(left_x);
  right_x = parseInt(right_x);
  upper_y = parseInt(upper_y);
  lower_y = parseInt(lower_y);
  
	this.TopLeft.Set(left_x, upper_y);
	this.BottomRight.Set(right_x, lower_y);
};
Freehand.prototype.AutoResize = function() {
  var i = 0;
  var bigX = 0;
  var bigY = 0;
  var littleX = 1000;
  var littleY = 1000;
  
  for( i = 0; i < this.points.length; i++) {
    if (this.points[i].GetX() > bigX) { bigX = this.points[i].GetX(); }
    if (this.points[i].GetY() > bigY) { bigY = this.points[i].GetY(); }
    if (this.points[i].GetX() < littleX) { littleX = this.points[i].GetX(); }
    if (this.points[i].GetY() < littleY) { littleY = this.points[i].GetY(); }
  }
  
  this.TopLeft.Set(littleX, littleY);
  this.BottomRight.Set(bigX, bigY);
  return this.BottomRight;
};

Freehand.prototype.toJSON = function ( container, color, add_point, bool_do_math ) {
  var arr = this.GetPoints();
  var jsonarr = new Array();
	var i;
  
  if (bool_do_math) {
  	var starter = ShapeType.GetPoint(container, add_point);
    	
  	for(i = 0; i < arr.length; i++) {
  		arr[i].Add(starter, arr[i]);
  		jsonarr[i] = arr[i].toJSON();		
  	}
  }
  else {
  	for(i = 0; i < arr.length; i++) {
  		jsonarr[i] = arr[i].toJSON();		
  	}
  }
  var ans = {
  		"Points" : jsonarr,
      "Counter" : this.counter,
      "LineWidth" : this.lineWidth
  	};
    return ans;

};
Freehand.prototype.GetDescription = function ( ) { return ''; };
Freehand.prototype.GetPoints = function() {
  var all_points = new Array();
  var i = 0;
  
  for(i = 0; i < this.counter; i++) {
    all_points[i] = new Point();
    all_points[i].Set(this.points[i].GetX(), this.points[i].GetY());
  }
  
	return all_points;
};






////////////////////// ShapeText //////////////////////////////

// First, set the rules for fonts
/**    1. 'serif' (e.g. Times)
    2. 'sans-serif' (e.g. Helvetica)
    3. 'cursive' (e.g. Zapf-Chancery)
    4. 'fantasy' (e.g. Western)
    5. 'monospace' (e.g. Courier)
    */

function FontTypes (num) { this.type = num; }
FontTypes.SERIF = 1;
FontTypes.SANSSERIF = 2;
FontTypes.CURSIVE = 3;
FontTypes.FANTASY = 4;
FontTypes.MONOSPACE = 5;

FontTypes.font_families = new Array();
FontTypes.font_families[FontTypes.SERIF] = "serif";
FontTypes.font_families[FontTypes.SANSSERIF] = "sans-serif";
FontTypes.font_families[FontTypes.CURSIVE] = "cursive";
FontTypes.font_families[FontTypes.FANTASY] = "fantasy";
FontTypes.font_families[FontTypes.MONOSPACE] = "monospace";



function ShapeText() {
  this.TopLeft = new Point();
  this.BottomRight = new Point();

  this.content = ShapeText.GetDefault();
  
  this.file = '';
  this.name = '';
  this.font_id = 0;
  this.size = 12;
  this.family = FontTypes.SERIF;
}

/**
ShapeText.fonts = new Array();

ShapeText.AddFont = function(file, name, default_size, shape_id, family_num) {
  ShapeText.fonts[shape_id] = new Array();
  ShapeText.fonts[shape_id]['family'] = family_num;
  ShapeText.fonts[shape_id]['name'] = name;
  ShapeText.fonts[shape_id]['default_size']  = default_size;
  ShapeText.fonts[shape_id]['file'] = file;
};
**/
ShapeText.fromJSON = function(inside_container) {
  var newshape = new ShapeText();
  newshape.content = inside_container.Text;
  newshape.SetFont(inside_container.File, inside_container.Size,inside_container.FontID, inside_container.Family, inside_container.Name );
  return newshape;
};
ShapeText.GetDefault = function() {
  return "Enter the text here";
};


ShapeText.prototype.SetFont = function( new_font_filename, new_font_size, new_font_shape_id, new_font_family, new_font_name ) {
  this.file = new_font_filename;
  this.size = new_font_size;
  this.font_id = new_font_shape_id;
  this.family = new_font_family;
  this.name = new_font_name;
};
ShapeText.prototype.GetFontID = function() {
  return this.font_id;
};
ShapeText.prototype.GetFontSize = function() {
  return this.size;
};
ShapeText.prototype.SetPoints = function(left_x, right_x, upper_y, lower_y) {
  left_x = parseInt(left_x);
  right_x = parseInt(right_x);
  upper_y = parseInt(upper_y);
  lower_y = parseInt(lower_y);
  
	this.TopLeft.Set(left_x, upper_y);
	this.BottomRight.Set(right_x, lower_y);
};
ShapeText.prototype.Calculate = function(canvas, x_padding, y_padding){ };
ShapeText.prototype.ChangeText = function(id, color, newtext){
  this.content = newtext;
  this.Draw(id,color);
};
ShapeText.prototype.Draw = function( id, color ) {
  var span = $("#"+id);
  var css = {
    'color' : color,
    'font-family' : "'" + this.name + "', " + FontTypes.font_families[this.family],
    'font-size' : this.size + "pt"
  };
  span.css(css);
  span.text(this.content);
};
ShapeText.prototype.GetDescription = function(color) {
  return this.content.substr(0,20);
};
ShapeText.prototype.toJSON = function( container, color, add_point, bool_do_math) {
  var points = new Array();
  
  if (bool_do_math) {
  	var starter = ShapeType.GetPoint(container, add_point );

    var newUpperLeft = new Point( 0, 0 );
  	var newLowerRight = new Point( 0, 0 );
    var blank = new Point(0, 0);
  	blank.Add(starter, newUpperLeft);
  	blank.Add(starter, newLowerRight);

    points = [
      newUpperLeft.toJSON(), 
      newLowerRight.toJSON() 
    ];
    
  	
  }
  else {
    points = [
      this.TopLeft.toJSON(),
      this.BottomRight.toJSON()
    ];
  }

  var ans = {
    "Points" : points,
    "Text" : this.content,
    "Size" : this.size,
    "File" : this.file,
    "Family" : this.family,
    "Name" : this.name,
    "FontID" : this.font_id
  };
    return ans;

};

function SymbolFixed (num) {this.type = num;}
SymbolFixed.BOTH = 0;
SymbolFixed.RATIO = 1;
SymbolFixed.NONE = 2;


function Symbol(){
  this.TopLeft = new Point();
  this.BottomRight = new Point();

  this.normal = new Object();
  this.normal.height = 1;
  this.normal.width = 1;
  
  this.file = '';
  this.name = '';
  this.shape_id = 0;
  this.height = 1;
  this.width = 1;
  this.dir ='';
  this.fixed = SymbolFixed.BOTH;

}


Symbol.fromJSON = function(inside_container) {
  var newshape = new Symbol();
  newshape.SetSymbol(inside_container.File, inside_container.Height,inside_container.Width, inside_container.Fixed, inside_container.ShapeID, inside_container.Dir );
  newshape.SetNormal(inside_container.NormalHeight, inside_container.NormalWidth);
  return newshape;
};


Symbol.prototype.SetSymbol = function( new_filename, new_height, new_width, new_fixed, new_shape_id, new_shape_dir ) {
  this.height = new_height;
  this.width = new_width;
  this.file = new_filename;
  this.fixed = new_fixed;
  this.shape_id = new_shape_id;
  this.dir = new_shape_dir;
  
  this.SetNormal(new_height, new_width);
};
Symbol.prototype.SetNormal = function( normal_height, normal_width ) {
  this.normal.height = normal_height;
  this.normal.width = normal_width;
};

Symbol.prototype.Calculate = function(canvas, x_padding, y_padding){};
Symbol.prototype.SetPoints =  function(left_x, right_x, upper_y, lower_y) {
  left_x = parseInt(left_x);
  right_x = parseInt(right_x);
  upper_y = parseInt(upper_y);
  lower_y = parseInt(lower_y);
  
	this.TopLeft.Set(left_x, upper_y);
	this.BottomRight.Set(right_x, lower_y);
};
Symbol.prototype.Draw = function(id, color) {
  var pic = $("#"+id);
//      'background-color' : color,
    var css = {
      'height' : this.height,
      'width' : this.width
    };
  pic.css(css);

  pic.attr("height", this.height);
  pic.attr("width", this.width);
  pic.attr("src", this.dir+this.file);
  
  pic.parent(".ui-wrapper").css(css);
};
Symbol.prototype.GetDescription = function(color) {};
Symbol.prototype.toJSON = function( container, color, add_point, bool_do_math) {
  var points = new Array();
  
  if (bool_do_math) {
  	var starter = ShapeType.GetPoint(container, add_point );

    var newUpperLeft = new Point( 0, 0 );
  	var newLowerRight = new Point( 0, 0 );
    var blank = new Point(0, 0);
  	blank.Add(starter, newUpperLeft);
  	blank.Add(starter, newLowerRight);

    points = [
      newUpperLeft.toJSON(), 
      newLowerRight.toJSON() 
    ];
    
  	
  }
  else {
    points = [
      this.TopLeft.toJSON(),
      this.BottomRight.toJSON()
    ];
  }

  var ans = {
    "Points" : points,
    "File" : this.file,
    "NormalHeight" : this.normal.height,
    "NormalWidth" : this.normal.width,
    "Dir" : this.dir,
    "Fixed" : this.fixed,
    "Height" : this.height,
    "Width" : this.width,
    "ShapeID" : this.shape_id
  };
    return ans;

};
