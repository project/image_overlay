
function Degrees (  ) { }
Degrees.standard = [0, 45, 90, 135, 180, 225, 270, 315 ];

function Radians () {}
Radians.standard = [0, (Math.PI / 4), (Math.PI / 2), ((3 * Math.PI)/ 4), Math.PI, ((5 * Math.PI)/ 4), ((3 * Math.PI)/ 2), ((7 * Math.PI)/ 4), (2 * Math.PI)];
 
/**	this.canvases[num] = ShapeCanvas.newShapeCanvas(new_color, type, new_container, new_canvas, left, right, top, bottom);
 
Shape.Facing = new Object();
Shape.Facing.NORTH = 0;
Shape.Facing.EAST = 1;
Shape.Facing.SOUTH = 2;
Shape.Facing.WEST = 3;
**/


/**
   * ShapeCanvas
   *   ShapeCanvas is a box which has in it the Canvas object. Each shape created through the GUI makes its own ShapeCanvas.
   */
function ShapeCanvas() {
	this.canvas_id = '';
	this.container_id = '';
	this.container = false;
  
  this.height = 0;
  this.width = 0;
	this.type = 0;
	this.color = "red";

	this.shape = false;
	this.x_padding = 2;
	this.y_padding = 1;
}

ShapeCanvas.newShapeCanvas = function(color, shape, div, canvas, left_x, right_x, upper_y, lower_y){
  var shapecanvas = new ShapeCanvas();
  shapecanvas.canvas_id = canvas;
	shapecanvas.container_id = div;
	shapecanvas.container = new Rect();
  
  shapecanvas.height = 0;
  shapecanvas.width = 0;
	shapecanvas.type = shape;
	shapecanvas.color = color;

	shapecanvas.shape = ShapeType.Classes[shapecanvas.type]();
	shapecanvas.x_padding = 2;
	shapecanvas.y_padding = 1;

	shapecanvas.SetContainer(left_x, right_x, upper_y, lower_y);
  return shapecanvas;
};
ShapeCanvas.fromJSON = function (json_object){
/** 
	return {
		"Type" : this.type,
    "TypeName" : name,
    "CanvasID" : this.canvas_id,
    "Color" : this.GetColor(),
	  "Container" : this.container.toJSON(blankC, color, false, false),
    "ContainerID" : this.container_id,
   /////////// "Absolute_Shape" : absolute_properties,
    "InContainer" : inside_container_properties
	};
**/
  var shapecanvas = new ShapeCanvas();
  
  shapecanvas.canvas_id = json_object.CanvasID;
	shapecanvas.container_id = json_object.ContainerID;
	shapecanvas.container = Rect.fromJSON(json_object.Container);
  
  shapecanvas.height = 0;
  shapecanvas.width = 0;
	shapecanvas.type = json_object.Type;
	shapecanvas.color = json_object.Color;

	shapecanvas.shape = ShapeType.fromJSON[shapecanvas.type](json_object.InContainer);
	shapecanvas.x_padding = 2;
	shapecanvas.y_padding = 1;

  var container = $("#"+shapecanvas.container_id);
  var canvas = $("#"+shapecanvas.canvas_id);
  
  var height = shapecanvas.container.GetBottomY() - shapecanvas.container.GetTopY();
  var width = shapecanvas.container.GetRightX() - shapecanvas.container.GetLeftX();
  
 var newCSS = {
    top : shapecanvas.container.GetTopY(),
    left :  shapecanvas.container.GetLeftX(),
    height : height,
    width :  width
  }
  
  container.css(newCSS);
  canvas.css(newCSS);

	canvas.attr("height", height);
	canvas.attr("width",width);  
  
	shapecanvas.SetContainer(shapecanvas.container.GetLeftX(), shapecanvas.container.GetRightX(), shapecanvas.container.GetTopY(), shapecanvas.container.GetBottomY());
  return shapecanvas;
};

ShapeCanvas.prototype.GetTypeName = function ( ) { return ShapeType.Names[this.type]; };

//type, id, color, x_padding, y_padding
ShapeCanvas.prototype.GetShapeType = function ( ) { return this.type; };


ShapeCanvas.prototype.GetText = function ( ) { return this.shape.GetDescription(); };
ShapeCanvas.prototype.GetFontID = function ( ) {
  if (this.type == ShapeType.TEXT) {
    return this.shape.GetFontID();
  }
  return 0;
 };
 ShapeCanvas.prototype.GetFontSize = function ( ) {
  if (this.type == ShapeType.TEXT) {
    return this.shape.GetFontSize();
  }
  return 0;
 };
ShapeCanvas.prototype.GetContainerID = function ( ) { return this.container_id(); };
ShapeCanvas.prototype.ChangeText = function (newtext) { 
  if (this.type == ShapeType.TEXT) {
    this.shape.ChangeText(this.canvas_id, this.color, newtext);
  }
};
ShapeCanvas.prototype.SetFont = function( new_font_filename, new_font_size, new_font_shape_id, new_font_family, new_font_name ) {
  if (this.type == ShapeType.TEXT) {
    this.shape.SetFont( new_font_filename, new_font_size, new_font_shape_id, new_font_family, new_font_name );
  }
};  

ShapeCanvas.prototype.SetSymbol = function( new_filename, new_height, new_width, new_fixed, new_shape_id, new_shpae_dir ) {
  if (this.type == ShapeType.SYMBOL) {
    this.shape.SetSymbol( new_filename, new_height, new_width, new_fixed, new_shape_id, new_shpae_dir );

  }
};  





ShapeCanvas.prototype.Calculate = function () {
  if (this.type == ShapeType.TEXT ) {
    var span = $("#"+this.container_id);
    var height = span.height();
    var width = span.width();
  }
  return this.shape.Calculate($("#"+this.canvas_id), this.x_padding, this.y_padding); };
ShapeCanvas.prototype.Draw = function ( ) { return this.shape.Draw(this.canvas_id, this.GetColor()); };
//Shape.prototype.GetDescription = function() { return ShapeType.Names[this.type] + " " + this.shapes.GetDescription(this.GetColor()); };

ShapeCanvas.prototype.toJSON = function( ) { 
  var blankC = new Object();
	var name = this.GetTypeName();
  var color = this.GetColor();
  var container = $("#"+this.container_id);

	var top = findInt(container.css("top"));
	var left = findInt(container.css("left"));


	var absolute_properties = this.shape.toJSON(this.container, color, false, true);
	var inside_container_properties = this.shape.toJSON(this.container, color, false, false);

	return {
		"Type" : this.type,
    "TypeName" : name,
    "CanvasID" : this.canvas_id,
    "Color" : this.GetColor(),
	  "Container" : this.container.toJSON(blankC, color, false, false),
    "ContainerID" : this.container_id,
    "Absolute_Shape" : absolute_properties,
    "InContainer" : inside_container_properties
	};
};



ShapeCanvas.prototype.ChangeMe = function ( ) {
	var canvas = $("#"+this.canvas_id);
	var container = $("#"+this.container_id);

	var top = findInt(container.css("top"));
	var left = findInt(container.css("left"));
	var height = findInt(container.css("height"));
	var width =  findInt(container.css("width"));
  if (this.type == ShapeType.TEXT || this.type == ShapeType.SYMBOL ) {
    var span = $("#"+this.container_id);
    var height = span.height();
    var width = span.width();
  }

	canvas.css("top", top);
	canvas.css("left", left);
	canvas.css("height", height);
	canvas.css("width", width);

	canvas.attr("height", height);
	canvas.attr("width",width);

	this.SetContainer(left, left+width, top, top+height);
	this.Draw(this.GetColor(), this.canvas_id);
}	


ShapeCanvas.prototype.MoveMe = function ( moveleft, movetop ) {
	var canvas = $("#"+this.canvas_id);
	var container = $("#"+this.container_id);

	var top = findInt(container.css("top"));
	var left = findInt(container.css("left"));
	var height = findInt(container.css("height"));
	var width =  findInt(container.css("width"));
  
  var newtop = top + movetop;
  var newleft = left + moveleft;
  
  container.css("top", newtop);
  container.css("left", newleft);


	this.SetContainer(left, left+width, top, top+height);
//	this.Draw(this.GetColor(), this.canvas_id);
}	

ShapeCanvas.prototype.ResizeMe = function( new_height, new_width, new_top, new_left ) {
	var canvas = $("#"+this.canvas_id);
	var container = $("#"+this.container_id);
	
	canvas.css("top", new_top);
	canvas.css("left", new_left);
	canvas.css("height", new_height);
	canvas.css("width", new_width);

	canvas.attr("height", new_height);
	canvas.attr("width", new_width);

	this.SetContainer(new_left, new_left+new_width, new_top, new_top+new_height);
	this.Draw(this.GetColor(), this.canvas_id);
	};

ShapeCanvas.prototype.SetColor = function(new_color) { this.color = new_color; };
ShapeCanvas.prototype.GetColor = function() { return this.color; };

ShapeCanvas.prototype.SetContainer = function (left_x, right_x, upper_y, lower_y) {
		this.container.SetPoints(left_x, right_x, upper_y, lower_y);
		
    var new_height = this.container.GetHeight();
    var new_width = this.container.GetWidth();
    
    if ( new_height < 0 || new_width < 0 ) {
      var item = $("#"+this.container_id);
      new_height = item.height();
      new_width = item.width();
    }
		
    if ( (new_height != this.height ) || (new_width != this.width) ) { 
      this.height = new_height;
      this.width = new_width;
      if ( this.type == ShapeType.FREEHAND ) {
        var tempcss = this.shape.ReCalculate( $("#"+this.canvas_id), new_height, new_width, left_x, upper_y );
        $("#"+this.canvas_id).css(tempcss).attr("height", tempcss.height).attr("width", tempcss.width);
        $("#"+this.container_id).css(tempcss).attr("height", tempcss.height).attr("width", tempcss.width);
      } else {
        this.SetArea(0, right_x - left_x, 0, lower_y - upper_y);
      }
      this.Draw();
    }
	};
ShapeCanvas.prototype.SetArea = function ( left_x, right_x, upper_y, lower_y ) {
	this.shape.Calculate( $("#"+this.canvas_id), this.x_padding, this.y_padding );
};

ShapeCanvas.prototype.SetLine = function( int_width ) {  
  if ( this.type == ShapeType.CIRCLE ) {
    this.shape.SetLineWidth( int_width );
    this.Draw();
  }
};

ShapeCanvas.prototype.SetNextPoint = function( int_x, int_y ) {
  if (this.type == ShapeType.FREEHAND){
    this.shape.SetNextPoint(int_x, int_y);
  }
};

ShapeCanvas.prototype.AddPoint = function( int_x, int_y ) {
  if (this.type == ShapeType.FREEHAND){
    this.shape.AddPoint(int_x, int_y, this.canvas_id,this.GetColor() );
  }
};
ShapeCanvas.prototype.AutoResize = function(){
    if (this.type == ShapeType.FREEHAND){
    var size = this.shape.AutoResize();
    this.SetContainer(0, size.GetX(), 0, size.GetY());
    }
};






//todo
//find formula for ellipses
// using bezeir curves??

function RemovePX(str) {
	var where = str.indexOf("px");
	ans = str.substring(0,where);
	ans *= 1;
	return ans;
};
