
standardDegreesToRadians = function ( num_degrees ) {
	switch ( num_degrees ) {
		case 0:
			return 0;
		case 45:
			return Math.PI / 4;
		case 90:
			return Math.PI / 2;
		case 135:
			return (3 * Math.PI)/ 4;
		case 180:
			return Math.PI;
		case 225:	
			return (5 * Math.PI)/ 4;
		case 270:
			return (3 * Math.PI)/ 2;
		case 315:
			return (7 * Math.PI)/ 4;
		case 360:
			return 2 * Math.PI;
		default:
			return 0;
	}

}

function max ( a, b ) {
	if ( a > b ) {
		return a;
	} else {
		return b;
	}
}


function min ( a, b ) {
	if ( a < b ) {
		return a;
	} else {
		return b;
	}
}


function RemovePX(str) {
	var where = str.indexOf("px");
	ans = str.substring(0,where);
	ans *= 1;
	return ans;
};



function findInt(str) {
	if ( typeof(str) == 'number') {return str;}
  str = String(str);
	var ans = parseInt(str);
	if ( isNaN(ans) ) {
    ans = str.match(/\d+/);
	}
	return ans * 1;
}