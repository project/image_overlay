/***
 *
 *   Custom.js
 *       Custom shapes, images and fonts
 *
 ***/

  function FontWindow(div_id, select_id, size_id) {
    this.div_id = div_id;
    this.select_id = select_id;
    this.size_id = size_id;
    this.current = new Object();
    this.fonts = new Array();
  }
  
  
  
  FontWindow.prototype.AddFont = function ( default_size, name, file, family_int, shape_id) {
  this.fonts[shape_id] = new Object();
    
    this.fonts[shape_id].size = default_size;
    this.fonts[shape_id].family = family_int;
    this.fonts[shape_id].name = name;
    this.fonts[shape_id].id = shape_id;
    this.fonts[shape_id].file = file;
    
//    $("#" + this.select_id).children('[value="' + shape_id + '"]');
};

FontWindow.prototype.SaveChanges = function() {
  var selector = $("#" + this.select_id).children(':selected');
  var id =  selector.val();
  this.current.id = id;
  this.current.size = $("#"+this.size_id).val();
  this.current.file = this.fonts[id].file;
  this.current.family = this.fonts[id].family;
  this.current.name = this.fonts[id].name;
  
  return this.fonts[id];
};

FontWindow.prototype.FontChanged = function() {
  var new_id = $("#" + this.select_id).children(':selected').val();
  $("#"+this.size_id).val(this.fonts[new_id].size);
  
};

FontWindow.prototype.CancelChanges = function() {
  $("#"+this.select_id).children("[value='"+this.current.id+"']").attr("selected", "selected");
  $("#"+this.size_id).val(this.current['size']);
};

function SymbolWindow(div_id, div_with_symbols_id, directory, pick_id, pick_class){
    this.div_id = div_id;
    this.pick_id = pick_id;
    this.pick_class = pick_class;
    this.directory = directory;
    this.symbols_div_id = div_with_symbols_id;
    this.current = new Object();
    this.symbols = new Array();
}



  SymbolWindow.prototype.AddSymbol = function ( name, file, height, width, fixed, shape_id) {
  this.symbols[shape_id] = new Object();
/***
      'id' => $image->shape_id,
      'name' => $image->shape_name,
      'height' => $image->shape_height,
      'width' => $image->shape_width,
      'fixed' => $image->fixed_size,
      'file' => $image->data

***/
  
    this.symbols[shape_id].width = width;
    this.symbols[shape_id].height = height;
    this.symbols[shape_id].name = name;
    this.symbols[shape_id].id = shape_id;
    this.symbols[shape_id].file = file;
    
//    $("#" + this.select_id).children('[value="' + shape_id + '"]');
};

  SymbolWindow.prototype.RenderSymbols = function ( ) {
    for( var i in this.symbols ) {
      if (this.symbols[i].id == i) {   //can get away with this since the first items added are fonts
      attrs = 'id="' + this.pick_id + i + '" ';
      attrs = attrs + 'height="' + this.symbols[i].height + '" ';
      attrs = attrs + 'width="' + this.symbols[i].width + '" ';
      attrs = attrs + 'class="' + this.pick_class + '" ';
      attrs = attrs + 'src="' + this.directory + this.symbols[i].file + '" ';
      var img = $('<img ' + attrs + ' />');
      var sym = this.symbols_div_id;
      img.click( function() {
        var d = $("#"+sym);
        var c = d.children(".selectedsymbol");
        c.removeClass("selectedsymbol");
        $(this).addClass("selectedsymbol");
      });
      img.appendTo($("#"+this.symbols_div_id));
    }
  }
  };

  SymbolWindow.prototype.SaveChanges = function ( ) {
  var shape_id = findInt($("#"+this.symbols_div_id).children(".selectedsymbol").attr("id"));

  this.current.id = shape_id;
  this.current.file = this.symbols[shape_id].file;
  this.current.height = this.symbols[shape_id].height;
  this.current.width = this.symbols[shape_id].width;
  this.current.fixed = this.symbols[shape_id].width;
  this.current.fixed = this.symbols[shape_id].width;
};

SymbolWindow.prototype.GetShapeID = function() {
  return this.current.id;
};
SymbolWindow.prototype.GetDirectory = function() {
  return this.directory;
};
