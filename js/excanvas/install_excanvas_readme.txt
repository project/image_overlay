This is where the excanvas files need to go if you want this module to work for IE users. The files are not included because they are released under the Apache License. 

The file structure should look like:
  .../image_overlay/js/excanvas/excanvas.js
  .../image_overlay/js/excanvas/excanvas-compressed.js

  
  excanvas may be downloaded from:
  http://excanvas.sourceforge.net/
