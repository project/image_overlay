These fonts were obtained from: 

http://savannah.gnu.org/projects/freefont/
http://www.gnu.org/software/freefont/
http://ftp.gnu.org/gnu/freefont/


These are release versions of the GNU FreeFont fonts.

You probably want to get just one of them, the one with the most recent date,
that matches your needs:

The "otf" files are OpenType, primarily for use in recent versions of Windows.

The "ttf" files are TrueType, for use in other operating systems.

The "sfd" files are FontForge "Spline Font Database" files, for use by
font developers.

The ".sig" files are an accountability requirement of the GNU upload
policies.  You probably don't need that.


