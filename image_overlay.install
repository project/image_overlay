<?php
/*
    1. 'serif' (e.g. Times)
    2. 'sans-serif' (e.g. Helvetica)
    3. 'cursive' (e.g. Zapf-Chancery)
    4. 'fantasy' (e.g. Western)
    5. 'monospace' (e.g. Courier)
*/


function image_overlay_install() {
  // Create my tables.
  drupal_install_schema('image_overlay');

   variable_set('image_overlay_ie_enable', 0);
  variable_set('image_overlay_temp_dir', 'files/temp/');
  
  db_query("INSERT INTO {overlay_shape_types} (shape_name, shape_type, shape_height, shape_width, data) VALUES ('Free Serif', 'font', 12, 1, 'FreeSerifA')"); 
  db_query("INSERT INTO {overlay_shape_types} (shape_name, shape_type, shape_height, shape_width, data) VALUES ('Free Sans', 'font', 12, 2, 'FreeSansY')"); 

  drupal_set_message(t('Image Overlay has been setup.'));
}

function image_overlay_uninstall() {
  // Drop my tables.
  drupal_uninstall_schema('image_overlay');
}



function image_overlay_schema( ) {

  $schema['overlays'] = array(
    'description' => t('A list of all the overlays on the site.'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node id for the node created by the overlay'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'original_nid' => array(
        'description' => t('The id of the first image that an overlay was created on that created this chain.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'overlay_id' => array(
        'description' => t('The id of this overlay.'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'data' => array(
        'description' => t('The serialized data representing the shapes.'),
        'type' => 'text',
        'size' => 'big',
      ),
    ),
    'indexes' => array(
      'nid' => array('nid'),
      'original_nid' => array('original_nid'),
    ),
    'primary key' => array('overlay_id'),
  );

  
  $schema['overlay_revisions'] = array(
    'description' => t('Revisions of each overlay'),
    'fields' => array(
      'nid' => array(
        'description' => t('The node id for the node created by the overlay'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'overlay_id' => array(
        'description' => t('The id of the overlay this revision belongs to.'),
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
      ),
      'timestamp' => array(
        'description' => t('The timestamp of the overlay this revision belongs to.'),
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ),
      'data' => array(
        'description' => t('The serialized data representing the shapes.'),
        'type' => 'text',
        'size' => 'big',
      ),
    ),
    'indexes' => array(
      'nid' => array('nid'),
      'overlay_id' => array('overlay_id'),
    ),
  );

  $schema['overlay_shape_types'] = array(
    'description' => t('An index of the types of shapes.'),
    'fields' => array(
      'shape_id' => array(
        'description' => t('The id for this type of shape'),
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
      'shape_name' => array(
        'description' => t('The name of this type of shape'),
        'type' => 'varchar',
        'length' => 50,
        'default' => '',
      ),
      'shape_type' => array(
        'description' => t('The custom shape types associated with this site. One of the following: "polygon", "font", "image". '),
        'type' => 'varchar',
        'length' => 50,
        'default' => '',
      ),
      'shape_height' => array(
        'description' => t('The height of an image, or the default font size'),
        'type' => 'int',
        'size' => 'normal',
      ),
      'shape_width' => array(
        'description' => t('The width of an image. The"type" of a font (see comment in header.'),
        'type' => 'int',
        'size' => 'normal',
      ),
      'fixed_size' => array(
        'description' => t('Can the aspect ratio or the size be changed? If no to both, 0. If yes to size, 1. If yes to both, 2. Ignored for text'),
        'type' => 'int',
        'size' => 'tiny',
      ),
      'data' => array(
        'description' => t('The font name (-.ttf) for "font". The image URL for "image". Info for "polygon".'),
        'type' => 'text',
        'not null' => TRUE,
      ), 
    ),
    'indexes' => array(
      'shape_type' => array('shape_type'),
      'shape_id' => array('shape_id'),
    ),
    'primary key' => array('shape_id'),
  );

  return $schema;
}


