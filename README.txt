

Image Overlay. 

Still in development.

Enable Image Overlay under modules, and then go to Administer >>  Site configuration >> Image Overlay. Make sure the directory exists. (There may be slight problems changing it using this interface). Then set permissions. Go to an image, and you should see a tab "Add Overlay". Clicking this will bring you to the create overlay form.

Note that this form will probably mess up your layout. 

Select a color and a shape, and hit "Add Shape". Part of a box will appear on the upper left hand side of the image. Drag it around, and the shape will become visible. Resize it, move it around, etc. Add as many shapes as you want, then hit "Prepare to make Image". DO NOT touch the shapes after hitting this. The results will not be saved. Hit "Draw" to submit the image. You will be directed to the new image node.


To use with IE, you need to download the Explorer Canvas package, from:
http://excanvas.sourceforge.net/
create a folder "excanvas" in image_overlay/js, and place the extracted zip files in that folder. 


The fonts included are licensed under the GPL, and can be found at :
http://www.gnu.org/software/freefont/